import numpy as np
import cv2 as cv

# def mouse callback function ####################################################
# it always has a default argument which is the trackbar position.
def Do_Nothing(x):
    if x < 100:
        cv.setTrackbarPos('0 : OFF \n1 : ON','image',0)
    else:
        cv.setTrackbarPos('0 : OFF \n1 : ON','image',1)
    
# Create a black image(3D Matrix 300x512x3 include BGR layors)
img = np.zeros((300,512,3), np.uint8)
cv.namedWindow('image')

# create trackbars for color change
cv.createTrackbar('R','image',0,255,Do_Nothing) # bind the bar to image window and callback
cv.createTrackbar('G','image',0,255,Do_Nothing)
cv.createTrackbar('B','image',0,255,Do_Nothing)
cv.setTrackbarPos('R','image',128)


# Create switch for ON/OFF functionality
switch = '0 : OFF \n1 : ON'
cv.createTrackbar(switch, 'image',0,1,Do_Nothing)

while(1):
    cv.imshow('image',img)
    k = cv.waitKey(1) & 0xFF
    if k == 27:
        break
    
    # get current positions of four trackbars
    r = cv.getTrackbarPos('R','image')
    g = cv.getTrackbarPos('G','image')
    b = cv.getTrackbarPos('B','image')

    s = cv.getTrackbarPos(switch,'image')
    if s == 0:
        img[:] = 0  # set image matix to 0
    else:
        img[:] = [b,g,r]    # set image layor to BGR
cv.destroyAllWindows()