
# Goal: ##########################################################################
# (1) Access pixel values and modify them
# (2) Access image properties
# (3) Setting Region of Interest (ROI)
# (4) Splitting and Merging images

import cv2 as cv
import numpy as np  # mainly making use of Numpy's array functionality
from matplotlib import pyplot as plt

import os
# Load Image #####################################################################
# Obtain absolute path of image file
img_path = os.path.join(os.getcwd(),'pyCV','imgs','Alzheimer.png')

# read image, default will be color image but remove alpha
# cv.IMREAD_GRAYSCALE = 0
# cv.IMREAD_COLOR =1, RGB with alpha
# cv.IMREAD_UNCHANGED ：1
img = cv.imread(img_path,cv.IMREAD_COLOR)
# Or Create a black image
#img = np.zeros((512,512,3), np.uint8)

# Image Pixel #####################################################################
# for array/region calculation, use numpy
print('BGR of pixel in 10x10:',img[10,10]) 
print('B of pixel in 10x10:',img[10,10,0])   # print B of pixel in 10x10
# for individual pixel access, usenumpy arrary methods 
print('B of pixel in 10x10:',img.item(10,10,0))   # B of pixel in 10x10
print('G of pixel in 10x10:',img.item(10,10,1))   # G of pixel in 10x10
print('R of pixel in 10x10:',img.item(10,10,2))   # R of pixel in 10x10

# Image Property ###################################################################
# number of rows, columns and channels, type of image data, number of pixels etc.
#   img.shape 
#       returns a tuple:（rows,columns,channels）if image is color.
print('(rows,columns,channels) =', img.shape )
print('Image Data Type:', img.dtype)

# read roi and set roi
start_row =100
start_col =100
roi = img[start_row:start_row+150, start_col:start_col+150]  # [row start:row end,col start:col end]
img[start_row:start_row+150, start_col:start_col+150] = roi+100
# split the BGR channels
b,g,r = cv.split(img)
b = img[:,:,0]
plt.subplot(337),plt.imshow(img,'gray'),plt.title('Blue')
#merge the BGR channels
img[:,:,2] = 0
img = cv.merge((b,g,r))
plt.subplot(338),plt.imshow(img,'gray'),plt.title('Merged')

# Image Border ###################################################################
# Use cv.copyMakeBorder() to create border, it takes following arguments:
#     （1）src： input image
#     （2）top, bottom, left, right：
#         border width in number of pixels in corresponding directions
#     （3）borderType： Flag defining what kind of border to be added. It can be following types:
#         cv.BORDER_CONSTANT： Adds a constant colored border. The value should be given as next argument.
#         cv.BORDER_REFLECT： mirror reflection of the border elements, like this: fedcba|abcdefgh|hgfedcb
#         cv.BORDER_REFLECT_101 or cv.BORDER_DEFAULT： Same as above, but with a slight change, like this: gfedcb|abcdefgh|gfedcba
#         cv.BORDER_REPLICATE： Last element is replicated throughout, like this: aaaaaa|abcdefgh|hhhhhhh
#         cv.BORDER_WRAP： Can't explain, it will look like this : cdefgh|abcdefgh|abcdefg
#     （4）value： Color of border if border type is cv.BORDER_CONSTANT
BLUE = [255,0,0]
replicate = cv.copyMakeBorder(img,10,10,10,10,cv.BORDER_REPLICATE)
reflect = cv.copyMakeBorder(img,10,10,10,10,cv.BORDER_REFLECT)
reflect101 = cv.copyMakeBorder(img,10,10,10,10,cv.BORDER_REFLECT_101)
wrap = cv.copyMakeBorder(img,10,10,10,10,cv.BORDER_WRAP)
constant= cv.copyMakeBorder(img,10,10,10,10,cv.BORDER_CONSTANT,value=BLUE)

plt.subplot(331),plt.imshow(img,'gray'),plt.title('ORIGINAL')
plt.subplot(332),plt.imshow(replicate,'gray'),plt.title('REPLICATE')
plt.subplot(333),plt.imshow(reflect,'gray'),plt.title('REFLECT')
plt.subplot(334),plt.imshow(reflect101,'gray'),plt.title('REFLECT_101')
plt.subplot(335),plt.imshow(wrap,'gray'),plt.title('WRAP')
plt.subplot(336),plt.imshow(constant,'gray'),plt.title('CONSTANT')
plt.show()