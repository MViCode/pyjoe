# INSTALL OPENCV #################################################################
# Windows:
#   pip install numpy
#   pip install matplotlib
#   pip install opencv_python
# Linux:
#   pip3 install numpy or apt-get install python3-numpy.
#   (You may need to apt-get install python3-pip.)
#   pip3 install matplotlib or apt-get install python3-matplotlib.
#   pip3 install python3-opencv or apt-get install python3-OpenCV
#     
import cv2 as cv
#from matplotlib import pyplot as plt    # mainly making use of Numpy's array functionality
#import numpy as np         # mainly making use of Numpy's array functionality

from tkinter import *
from tkinter import ttk
# import Pillow, a simple Python_based imaging library
# install Pillow by "pip install Pillow"
from PIL import Image, ImageTk 
import os

# Load Image ###################################################################
# Obtain absolute path of image file
img_file='Lenna.png'
img_path = os.path.join(os.getcwd(),'pyCV','imgs', img_file)

# read image, default will be color image but remove alpha
# cv2.IMREAD_GRAYSCALE = 0
# cv2.IMREAD_COLOR =1, RGB with alpha
# cv2.IMREAD_UNCHANGE =-1
img = cv.imread(img_path,cv.IMREAD_COLOR)

# cv2.imshow('CV show example', img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

img=cv.cvtColor(img,cv.COLOR_BGR2RGB); # or cv2.COLOR_BGR2RGB
#Rearrang the color channel
#b,g,r = cv2.split(img)
#img = cv2.merge((r,g,b))

# A root window for displaying objects
root = Tk()
root.geometry("1048x768")

SrcFrame = ttk.Labelframe(root,text="Source Image:")
DstFrame = ttk.Labelframe(root,text="Destination Image:")
CtrlFrame = ttk.Labelframe(root,text="Control Pannel:")
DstFrame.grid(row=1,column=2)
SrcFrame.grid(row=1,column=1)
CtrlFrame.grid(row=2,column=1,columnspan=2)

img = Image.fromarray(img)          # Convert the Image to PIL format
imgtk = ImageTk.PhotoImage(img)     #convert image to imageTk format 

labelImg_Src = ttk.Label(SrcFrame)
labelImg_Dst = ttk.Label(DstFrame)
labelImg_Src.grid(row=0,column=0)
labelImg_Dst.grid(row=0,column=0)
labelImg_Src.configure(text="Src",image=imgtk,compound="bottom")
labelImg_Dst.configure(text="Dst",image=imgtk,compound="bottom")

def OnBtnChecked():
    pass

btn=ttk.Button(CtrlFrame,text="Image Processing...",command=OnBtnChecked)
btn.grid(row=0, column=0)


root.mainloop() # Start the GUI