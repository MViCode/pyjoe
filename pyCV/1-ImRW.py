# INSTALL OPENCV #################################################################
# Windows:
#   pip install numpy
#   pip install matplotlib
#   pip install opencv_python
# Linux:
#   pip3 install numpy or apt-get install python3-numpy.
#   (You may need to apt-get install python3-pip.)
#   pip3 install matplotlib or apt-get install python3-matplotlib.
#   pip3 install python3-opencv or apt-get install python3-OpenCV
#     
import cv2 as cv
# import numpy as np         # mainly making use of Numpy's array functionality
# from matplotlib import pyplot as plt    # mainly making use of Numpy's array functionality
import os
# Load Image ###################################################################
# Obtain absolute path of image file
img_file='Alzheimer.png'
pwd = os.getcwd()
img_path = os.path.join(pwd,'pyCV','imgs', img_file)

# read image, default will be color image but remove alpha
# cv.IMREAD_GRAYSCALE = 0
# cv.IMREAD_COLOR =1, RGB with alpha
# cv.IMREAD_UNCHANGED =-1
img0 = cv.imread(img_path,cv.IMREAD_GRAYSCALE)    # Grayscale image
img1 = cv.imread(img_path,cv.IMREAD_COLOR)        # Color in BGR format without alpha
img_1 = cv.imread(img_path,cv.IMREAD_UNCHANGED)   # no touch (incl. alpha if have)

cv.namedWindow('IMREAD_GRAYSCALE',cv.WINDOW_GUI_NORMAL)
cv.imshow('IMREAD_GRAYSCALE', img0)
cv.namedWindow('IMREAD_COLOR',cv.WINDOW_AUTOSIZE)
cv.imshow('IMREAD_COLOR',img1)
cv.namedWindow('IMREAD_UNCHANGED',cv.WINDOW_GUI_EXPANDED)
cv.imshow('IMREAD_UNCHANGED',img_1)

# #plot with matplotlib:
# plt.imshow(img0, cmap = 'gray', interpolation = 'bicubic')
# plt.xticks([])
# plt.yticks([])  # to hide tick values on X and Y axis
# plt.show()

key=cv.waitKey(0) & 0xFF    # Add 0xFF if it is 64bit machine
if key == 27:   # Esc to quit
    cv.destroyAllWindows()
elif key == ord('g'):   # close gray window
    cv.destroyWindow('IMREAD_GRAYSCALE')
elif key == ord('s'):   # save gray file
    cv.imwrite('pyCV/imgs/test.png',img0)
    cv.destroyAllWindows()
else:
    pass
