# INSTALL OPENCV #################################################################
# Windows:
#   pip install numpy
#   pip install matplotlib
#   pip install opencv_python
# Linux:
#   pip3 install numpy or apt-get install python3-numpy.
#   (You may need to apt-get install python3-pip.)
#   pip3 install matplotlib or apt-get install python3-matplotlib.
#   pip3 install python3-opencv or apt-get install python3-OpenCV
#     
import cv2 as cv
import numpy as np         # mainly making use of Numpy's array functionality
# from matplotlib import pyplot as plt    # mainly making use of Numpy's array functionality

drawing = False # true if mouse is pressed
mode = True # if True, draw rectangle. Press 'm' to toggle to curve
ix,iy = -1,-1

# def mouse callback function ####################################################
# use following 3 line of code  list all available mouse events:
# import cv2 as cv
# events = [i for i in dir(cv) if 'EVENT' in i]
# print( events )
#    ['EVENT_FLAG_ALTKEY', 'EVENT_FLAG_CTRLKEY', 'EVENT_FLAG_LBUTTON', 
#    'EVENT_FLAG_MBUTTON','EVENT_FLAG_RBUTTON', 'EVENT_FLAG_SHIFTKEY', 
#    'EVENT_LBUTTONDBLCLK', 'EVENT_LBUTTONDOWN', 'EVENT_LBUTTONUP', 
#    'EVENT_MBUTTONDBLCLK', 'EVENT_MBUTTONDOWN', 'EVENT_MBUTTONUP', 
#    'EVENT_MOUSEHWHEEL', 'EVENT_MOUSEMOVE', 'EVENT_MOUSEWHEEL', 
#    'EVENT_RBUTTONDBLCLK', 'EVENT_RBUTTONDOWN', 'EVENT_RBUTTONUP']
# Creating mouse callback function has a specific format which is same everywhere. 
# It differs only in what the function does.
def draw_circle(event,x,y,flags,param):     # (1) define the callback
    global ix,iy,drawing,mode
    if event == cv.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y
    elif event == cv.EVENT_MOUSEMOVE:
        if drawing == True:
            if mode == True:
                cv.rectangle(img,(ix,iy),(x,y),(0,255,0),-1)
            else:
                cv.circle(img,(x,y),5,(0,0,255),-1)
    elif event == cv.EVENT_LBUTTONUP:
        drawing = False
        if mode == True:
            cv.rectangle(img,(ix,iy),(x,y),(0,255,0),-1)
        else:
            cv.circle(img,(x,y),5,(0,0,255),-1)
    elif event == cv.EVENT_LBUTTONDBLCLK:
        cv.circle(img,(x,y),100,(255,0,0),-1)
    else:
        pass

# Create a black image, a window and bind the function to window
img = np.zeros((512,512,3), np.uint8)
cv.namedWindow('image')
cv.setMouseCallback('image',draw_circle)    # (2) bind callback

while(1):   # use 1ms so that image display can be updated
    cv.imshow('image',img)
    key=cv.waitKey(1) & 0xFF    # Add 0xFF if it is 64bit machine
    if key == 27:   # Esc to quit
        break
    elif key == ord('m'):  
        mode = not mode
    elif key == ord('s'):   # save gray file
        cv.imwrite('pyCV/imgs/test.png',img0)
    else:
        pass
cv.destroyAllWindows()


