# INSTALL OPENCV #################################################################
# Windows:
#   pip install numpy
#   pip install matplotlib
#   pip install opencv_python
# Linux:
#   pip3 install numpy or apt-get install python3-numpy.
#   (You may need to apt-get install python3-pip.)
#   pip3 install matplotlib or apt-get install python3-matplotlib.
#   pip3 install python3-opencv or apt-get install python3-OpenCV
#     
import cv2 as cv
import numpy as np         # mainly making use of Numpy's array functionality
# from matplotlib import pyplot as plt    # mainly making use of Numpy's array functionality
import os
# Load Image #####################################################################
# Obtain absolute path of image file
img_file='Alzheimer.png'
pwd = os.getcwd()
img_path = os.path.join(pwd,'pyCV','imgs', img_file)

# read image, default will be color image but remove alpha
# cv.IMREAD_GRAYSCALE = 0
# cv.IMREAD_COLOR =1, RGB with alpha
# cv.IMREAD_UNCHANGED =-1
img = cv.imread(img_path,cv.IMREAD_COLOR) 
# Or Create a black image
#img = np.zeros((512,512,3), np.uint8)

# Draw functions #################################################################
# cv.line(), cv.circle() , cv.rectangle(), cv.ellipse(), cv.putText() etc.
# some common arguments
#   - img : The image where you want to draw the shapes
#   - color : 
#       Color of the shape. 
#       for BGR, pass it as a tuple, eg: (255,0,0) for blue. 
#       For grayscale, just pass the scalar value.
#   - thickness :
#       Thickness of the line or circle etc. 
#       If **-1** is passed for closed figures like circles, it will fill the shape.
#       default thickness = 1
#   - lineType : 
#       Type of line, whether 8-connected, anti-aliased line etc. 
#       By default, it is 8-connected. 
#       cv.LINE_AA gives anti-aliased line which looks great for curves.

# Draw a diagonal blue line with thickness of 5 px
cv.line(img,(0,0),(511,511),(255,0,0),5)
# top-left, bottom-right,
cv.rectangle(img,(384,0),(510,128),(0,255,0),3)
cv.circle(img,(447,63), 63, (0,0,255), -1)

# To draw the ellipse, need to pass several arguments. 
#   -center location (x,y).
#   - axes lengths (major axis length, minor axis length).
#   - angle of rotation of ellipse in anti-clockwise direction.
#   - startAngle and endAngle denotes the starting and ending 
#     of ellipse arc measured in clockwise direction from major axis. 
#     i.e. giving values 0 and 360 gives the full ellipse. 
#   Below example draws a half ellipse at the center of the image.
cv.ellipse(img,(256,256),(100,50),0,0,180,255,-1)

# To draw a polygon, first you need coordinates of vertices. 
# Make those points into an array of shape ROWSx1x2 where ROWS are number of 
# vertices and it should be of type int32. 
# Here we draw a small polygon of with four vertices in yellow color. 
pts = np.array([[10,5],[20,30],[70,20],[50,10]], np.int32)
pts = pts.reshape((-1,1,2))
cv.polylines(img,[pts],False,(0,0,255),3)
# cv.polylines() can be used to draw multiple lines. 
# Just create a list of all the lines you want to draw and pass it to the function. 
# All lines will be drawn individually. 
# It is a much better and faster way to draw a group of lines than calling 
# cv.line() for each line.


font = cv.FONT_HERSHEY_PLAIN
cv.putText(img,'Put Text',(10,200), font, 4,(0,0,128),4,cv.LINE_AA)

cv.namedWindow('Src Image',cv.WINDOW_AUTOSIZE)
cv.imshow('Src Image', img)

key=cv.waitKey(0) & 0xFF    # Add 0xFF if it is 64bit machine
if key == 27:   # Esc to quit
    cv.destroyAllWindows()
elif key == ord('g'):   # close gray window
    cv.destroyWindow('Src Image')
elif key == ord('s'):   # save gray file
    cv.imwrite('pyCV/imgs/test.png',img0)
    cv.destroyAllWindows()
else:
    pass