# INSTALL OPENCV #################################################################
# Windows:
#   pip install numpy
#   pip install matplotlib
#   pip install opencv_python
# Linux:
#   pip3 install numpy or apt-get install python3-numpy.
#   (You may need to apt-get install python3-pip.)
#   pip3 install matplotlib or apt-get install python3-matplotlib.
#   pip3 install python3-opencv or apt-get install python3-OpenCV
#     
import cv2 as cv
# import numpy as np         # mainly making use of Numpy's array functionality
# from matplotlib import pyplot as plt    # mainly making use of Numpy's array functionality
import os
# Load Image ###################################################################
# Obtain absolute path of image file
pwd = os.getcwd()
video_src_path = os.path.join(pwd,'pyCV','Videos', 'aoi.mp4')

cap = cv.VideoCapture(video_src_path)    # if read from camera, use cv.VideoCapture(0)

# Define the codec and create VideoWriter object
fourcc = cv.VideoWriter_fourcc(*'MJPG') # MJPG(.mp4), DIVX(.avi) as per fourcc.org
video_dst_path = os.path.join(pwd,'pyCV','Videos', 'output.mp4')
frm_W=int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
frm_H=int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
out = cv.VideoWriter(video_dst_path,fourcc,25.0,(frm_W,frm_H),isColor=True)

while cap.isOpened():
    # Capture frame-by-frame
    ret, frame = cap.read()
    # if frame is read correctly ret is True
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    
    # Our operations on the frame come here
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    frame = cv.flip(gray, 0)
    # write the flipped frame
    out.write(frame)
    # Display the resulting frame
    cv.imshow('frame', frame)

    #cap.get() or cap.set() to access property
    cap.set(cv.CAP_PROP_FRAME_HEIGHT,500)
    print(cap.get(cv.CAP_PROP_FRAME_COUNT))
    
    key=cv.waitKey(40) &0xFF
    if key == ord('q') or key==27:   # wait 40ms
        break
    elif key == ord('s'):   # save one frame
        cv.imwrite('pyCV/imgs/test.png',img0)

else:
    print("Cannot open camera")
    exit()
# When everything done, release the capture
cap.release()
out.release()
cv.destroyAllWindows()