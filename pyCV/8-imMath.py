
# Goal: ##########################################################################
# (1) Image Addition
# (2) Image Blending

import cv2 as cv
import numpy as np  # mainly making use of Numpy's array functionality
from matplotlib import pyplot as plt

import os
# Load Image #####################################################################
# Obtain absolute path of image file
pwd = os.getcwd()
path_img1 = os.path.join(pwd,'pyCV','imgs','eye.png')
path_img2 = os.path.join(pwd,'pyCV','imgs','brain.png')

# read image, default will be color image but remove alpha
# cv.IMREAD_GRAYSCALE = 0
# cv.IMREAD_COLOR =1, RGB with alpha
# cv.IMREAD_UNCHANGED ：1
img1 = cv.imread(path_img1,cv.IMREAD_GRAYSCALE)
img2 = cv.imread(path_img2,cv.IMREAD_COLOR)
# Or Create a black image
#img = np.zeros((512,512,3), np.uint8)


# Image Operation ##############################################################
# (1) Addition:
# Both images should be of same depth and type,
# or second image can just be a scalar value.
x = np.uint8([250])
y = np.uint8([10])
print('CV saturated operation:',cv.add(x,y))  # saturated operation, 250+10 =260=> 255
print('Numpy modulo operation:',x+y)  # Numpy modulo operation, 250+10 =260%256=>4

img2gray = cv.cvtColor(img2,cv.COLOR_BGR2GRAY)
dst_add=cv.add(img1,img2gray)
# different weights are given to images to give a feeling of blending or transparency
# dst = α⋅img1 + β⋅img2 + γ, β=1-α, 
dst_blend=cv.addWeighted(img1,0.7,img2gray,0.3,0)

 #(2) Bitwise Operations #######################################################
 # includes bitwise AND, OR, NOT and XOR operations
 # used to extracting any part of the image, defining and working with non-rectangular ROI etc.
 
 # Create ROI
 
rows,cols,channels = img2.shape
roi = img1[0:rows, 0:cols]


# Now create a mask and create its inverse mask also
ret, mask = cv.threshold(img2gray, 130, 255, cv.THRESH_BINARY) # image2 is grayscale image
mask_inv = cv.bitwise_not(mask)

# Now black-out the area of logo in ROI
img1_bg = cv.bitwise_and(roi,roi,mask = mask_inv)
# Take only region of logo from logo image.
img2_fg = cv.bitwise_and(img2gray,img2gray,mask = mask)
# Put logo in ROI and modify the main image
dst = cv.add(img1_bg,img2_fg)
img1[0:rows, 0:cols ] = dst


plt.subplot(331),plt.imshow(img1,'gray'),plt.title('Image1-70%')
plt.subplot(332),plt.imshow(img2,'gray',interpolation = 'bicubic'),plt.title('Image2-30%')
plt.subplot(333),plt.imshow(dst_add,'gray'),plt.title('Addition')
plt.subplot(334),plt.imshow(dst_blend,'gray'),plt.title('Blended')
plt.subplot(335),plt.imshow(mask,'gray'),plt.title('mask')
plt.show()
