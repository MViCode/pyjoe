# import Libs
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from array import *
from FoodSetup import *

root = Tk()

# Set the "borderwidth" configuration option (which defaults to 0, so no border), 
# "relief" option specifies the border appearance: "flat" (default), "raised", "sunken", "solid", "ridge", or "groove". 
FrameContainer = ttk.Frame(root, relief="flat")
FrameContainer.grid(row=0,column=0,sticky=(N, W, E, S))
FrameContainer.grid_columnconfigure(0, weight=1)
FrameContainer.grid_rowconfigure(0,weight=1)

#Create Menu Frame for Buttons
#padding= left, top, right and bottom padding
FrameMenu = ttk.Labelframe(FrameContainer,text="Menu",padding="10 10 10 10",borderwidth=1, relief="solid")
FrameMenu.grid(row=0,column=0,rowspan=3,sticky=(N, W, E, S),padx=5)

#Create Food Frame for input Boxes
frameFood = ttk.Labelframe(FrameContainer,text="Yami",padding="10 10 10 10",borderwidth=1,relief="solid")
frameFood.grid(row=0,column=1,rowspan=3, columnspan=2,sticky=(N, W, E, S),padx=5)

# (1)Listbox ==============================================================================================================================================
FoodClass = StringVar(value=FoodCategory)
menuList = Listbox(FrameMenu,listvariable=FoodClass, height=33)
menuList.grid(row=0,column=0,sticky=(N, W, E, S))
# Colorize alternating lines of the listbox
for i in range(0,len(FoodCategory),2):
    menuList.itemconfigure(i, background='#f0f0ff')

#Load Images to memory
matrixImages = []
for j in range(len(FoodName[0])):
    matrixImages.append(PhotoImage(file=(FoodName[0][j]+'.png')))

#create place holders & Initialize the display
matrixPlace = []
for x in range(len(FoodName[0])):
    matrixPlace.append(ttk.Label(frameFood))
    matrixPlace[x].grid(row = x//3,column=x%3)
    matrixPlace[x].configure(text=FoodName[0][x],image=matrixImages[x],compound="bottom")

#Display Food in selected category
def DisplaySelectedCategory(*args):
    global matrixImages,matrixPlace
    idxs = menuList.curselection()
    if len(idxs)==1:
        idx = int(idxs[0])
        #messagebox.showinfo("Information",FoodCategory[idx])
        for j in range(len(FoodName[idx])):
            matrixImages[j]=PhotoImage(file=(FoodName[idx][j]+'.png'))
        for i in range(len(FoodName[idx])):
            matrixPlace[i].configure(text=FoodName[idx][i],image=matrixImages[i],compound="bottom")

menuList.bind('<<ListboxSelect>>', DisplaySelectedCategory)















# Run mainloop
root.mainloop()