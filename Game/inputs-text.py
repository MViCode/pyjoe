import pgzrun
from random import randint

# Game screen size
WIDTH = 1500    
HEIGHT = 900

MAX_rSPEED=10
rSpeed=0
MIN_rSPEED=-10

ShipFlag=1      # use to flag the ship images


# Load Actor from ./images folder =========================================================
ship = Actor("ship1",anchor=('center','center'))
ship.pos = WIDTH/2,HEIGHT/2
ship.angle = 0

# =========================================================================================
'''Overwrite pgzero build in function for GUI redraw in following situation:
    > If you have defined an update() function (see below).
    > If a clock event fires.
    > If an input event has been triggered.
    Note: if you attempt to modify or animate something within the draw function
    the actor is not guaranteed to continue moving across the screen.The correct way is
    to uses update() to modify or animate actor and draw actor in draw(). 
'''
def draw():
    screen.clear()
    #screen.fill('green')
    # screen.fill((100,200,100))
    screen.blit('bg0',(0,0))
    ship.draw()   # draw actor at its current position
    screen.draw.text("Roate Spped: %d"%rSpeed,color='light blue',topleft=(10,HEIGHT-10))
    if rSpeed==MAX_rSPEED or rSpeed==MIN_rSPEED:
        screen.draw.text("Reached Speed Limit!",color='red',topleft=(10,40))

# Overwrite pgzero build in function repeatedly(60times/sec) to setup game logic
# update() will repeat (60times/sec);
# update(dt) will repeat evey dt time 
def update():
    On_Key_Press() 
    ship.angle +=rSpeed


# Handle keyboard Press ===================================================================
def On_Key_Press():     # Press simulation,support continous press
    if keyboard.left:
        ship.x -= 2
        if ship.left < 0:
            ship.left=WIDTH
    elif keyboard.right:
        ship.x += 2
        if ship.right > WIDTH:
            ship.right=0
    elif keyboard.up:
        ship.y -= 2
        if ship.top < 0:
            ship.top=HEIGHT
    elif keyboard.down:
        ship.y += 2
        if ship.bottom >= HEIGHT:
            ship.bottom=0
    elif keyboard.ESCAPE:
        quit()    # quit the game
    else:
        pass

def on_key_down(key,mod):   # Accurate control, on_key_up
    global rSpeed
    if key == keys.ESCAPE:  # use key parameter to capture the keys
        quit()
    if keyboard.PAGEUP:     # use keyboard object to capture the keys
        rSpeed +=1
        if rSpeed >= MAX_rSPEED:
            rSpeed=MAX_rSPEED
    if keyboard.PAGEDOWN:
        rSpeed -=1
        if rSpeed <= MIN_rSPEED:
            rSpeed=MIN_rSPEED
    if key == keys.C and mod == keymods.LCTRL:
        global ShipFlag
        if ShipFlag == 1:
            ship.image='ship2'
            ShipFlag=2
        elif ShipFlag == 2:
            ship.image ='ship3'
            ShipFlag=3
        elif ShipFlag ==3:
            ship.image='ship1'
            ShipFlag=1
        else:
            pass
        
# Handle Mouse Clicks =======================================================================
def set_ship_normal():
    ship.image = 'ship1'

'''
Overwrite pgzero build in function for mouse click
    > on_mouse_down([pos][, button])
    > on_mouse_up([pos][, button])
    > on_mouse_move([pos][, rel][, buttons])
    [pos] – A tuple (x, y) that gives the location that the mouse pointer moved to.
    [rel] – A tuple (delta_x, delta_y) that represent the change in the mouse pointer’s position.
    [button] – A mouse enum value indicating the button that was released.
    [buttons] – A set of mouse enum values indicating the buttons that were depressed during the move.
'''
def on_mouse_down(pos,button):
    global rSpeed
    if button == mouse.LEFT:
        # test if mouse click is inside of actor rect
        # can also use colliderect(Rect) to test if rect overlap with actor rect.
        if ship.collidepoint(pos): 
            ship.image ='ship3'   # change image of actor
        else:
            ship.pos=pos
    elif button==mouse.RIGHT:
        if ship.collidepoint(pos): 
            ship.image ='base1'   # change image of actor
            sounds.eep.play() # play sound
            #ship.x=randint(10, WIDTH-10)
            #ship.y=randint(10, HEIGHT-10)
            ship.pos=randint(10, WIDTH-10),randint(10, HEIGHT-10)
            #set ship image back to normal after 0.5sec, also prevent
            #function being scheduled more than once,e.g. if you keep click.
            clock.schedule_unique(set_ship_normal,0.5)
        else:
            # Actor.angle_to(target) returns angle(degree) from actor’s position to target.
            ship.angle -= ship.angle_to(pos)
            # Actor.distance_to(target) returns the distance from actor’s (in pixels) to target.
             
    elif button==mouse.WHEEL_UP: 
        rSpeed +=1
        if rSpeed >= MAX_rSPEED:
            rSpeed=MAX_rSPEED
    elif button==mouse.WHEEL_DOWN:
        rSpeed -=1
        if rSpeed <= MIN_rSPEED:
            rSpeed=MIN_rSPEED
    else:
        pass

def on_mouse_move(pos,rel,buttons):
    if  mouse.LEFT in buttons and ship.collidepoint(pos):
        ship.pos=pos
    else:
        ship.anchor=('center', 'center')
        ship.angle = ship.angle_to(pos)




pgzrun.go()

