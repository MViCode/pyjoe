import pgzrun
from random import randint

# Game screen size
WIDTH = 1300    
HEIGHT = 850

IS_LEFT_DRAW = False
IS_RIGHT_DRAW = False

# Load Actor from ./images folder =========================================================
soilder = Actor("soilder2",anchor=('center','center'))
soilder.pos = WIDTH/2+50,HEIGHT/2+50


# Build-in Draw() Object =====================================================================
# #Overwrite pgzero build in function for GUI redraw in following situation:
#     > If you have defined an update() function (see below).
#     > If a clock event fires.
#     > If an input event has been triggered.
#     Note: if you attempt to modify or animate something within the draw function
#     the actor is not guaranteed to continue moving across the screen.The correct way is
#     to uses update() to modify or animate actor and draw actor in draw(). 
def draw():
        if IS_LEFT_DRAW:
            on_left_draw()
        elif IS_RIGHT_DRAW:
            on_right_draw()
        else:
            draw_bg()

# Build-in Update() Object ====================================================================
# Overwrite pgzero build in function repeatedly(60times/sec) to setup game logic
# update() will repeat (60times/sec);
# update(dt) will repeat evey dt time 
def update():
    pass


# Build-in Screen Objecte =====================================================================
def draw_bg():
    screen.clear()
    #screen.fill('green')
    # screen.fill((100,20,0)
    screen.blit('bg1',(0,0))
    soilder.draw()   # drIS_RIGHT_DRAW

def on_left_draw():
    screen.blit('alien', (soilder.x+10,soilder.y-60))        # another way to draw the actors
    screen.draw.line((soilder.x+10,soilder.y-60),(soilder.x+450,soilder.y+100),"white")
    screen.draw.circle((soilder.x+10,soilder.y-60),10,"white")
    screen.draw.filled_circle((soilder.x+450,soilder.y+100),30,"white")
    # define box with Rect((left, top), (width, height)) or Rect(left, top, width, height)
    BOX = Rect((soilder.x+10,soilder.y-60),(450+10,100+60))
    screen.draw.rect(BOX,"white")
    screen.draw.filled_rect(Rect(soilder.x+10,soilder.y-60,-100,-100),"white")


    # Textbox and text
    screen.draw.textbox("Text Box", (0, 0,300,100), color="light blue")
    screen.draw.text("Text color", (50, 30), color="orange")
    #screen.draw.text("Font name and size", (20, 100), fontname="Boogaloo", fontsize=60)
    screen.draw.text("Positioned text", topright=(840, 20))
    screen.draw.text("Allow me to demonstrate wrapped text.", (90, 210), width=180, lineheight=1.5)
    screen.draw.text("Outlined text", (400, 70), owidth=1.5, ocolor=(255,255,0), color=(0,0,0))
    screen.draw.text("Drop shadow", (640, 110), shadow=(2,2), scolor="#202020")
    screen.draw.text("Color gradient", (540, 170), color="red", gcolor="purple")
    screen.draw.text("Transparency", (700, 240), alpha=0.1)
    screen.draw.text("Vertical text", midleft=(40, 440), angle=90)
    # screen.draw.text("All together now:\nCombining the above options",
    #     midbottom=(427,460), width=360, fontname="Boogaloo", fontsize=48,
    #     color="#AAFF00", gcolor="#66AA00", owidth=1.5, ocolor="black", alpha=0.8)

# Build-in image Objecte =====================================================================
# Each loaded image is a Pygame Surface,can typically use screen.blit(...) to draw to the screen.
# It also provides handy methods to query the size of the image in pixels:
# class Surface:
#     get_width()
#         Returns the width of the image in pixels.
#     get_height()
#         Returns the height of the image in pixels.
#     get_size()
#         Returns a tuple (width, height) indicating the size in pixels of the surface.
#     get_rect()
#         Get a Rect(left, top, width, height) that is pre-populated with the bounds of 
#         the image if the image was located at the origin.equivalent to:
#         Rect((0, 0), image.get_size())
def on_right_draw():
    mushrooms=[]
    for i in range(6):
        mushrooms.append(Actor('mushroom%d'%(i+1),topleft=(images.mushroom1.get_width()*i,0)))
    for x in mushrooms:
        x.draw()

def on_mouse_down(pos,button):
    global IS_LEFT_DRAW, IS_RIGHT_DRAW
    if button == mouse.LEFT:
        IS_LEFT_DRAW = True
    elif button == mouse.RIGHT:
        IS_RIGHT_DRAW = True
    else:    
        pass
def on_mouse_up(pos,button):
    global IS_LEFT_DRAW, IS_RIGHT_DRAW
    if button == mouse.LEFT:
        IS_LEFT_DRAW = False
    elif button == mouse.RIGHT:
        IS_RIGHT_DRAW = False
    else:    
        pass
    
# sound object ==========================================================================
# Support .wav and .ogg formats, load them to memory one time.
# WAV is great for small sound,
# OGG is a compressed format that is more suited to music.
# class Sound:
#     play()
#         Play the sound.
#     play(loops)
#         Play the sound, but loop it a number of times.
#         Parameters:	loops – The number of times to loop. 
#         if loop = -1 the sound will loop forever (or until you call Sound.stop()
#     stop()
#         Stop playing the sound.
#     get_length()
#         Get the duration of the sound in seconds.


# Music object ==========================================================================
#  Support .mp3, wav, ogg
# （1）The music system will load the track a little bit at a time while the music plays,
# avoiding the problems with using sounds to play longer tracks.
# （2）only one music track can be playing at a time. If you play a different track, 
# the previously playing track will be stopped.
# music.play(name)
#     Play a music track from the given file. The track will loop indefinitely.
#     This replaces the currently playing track and cancels any tracks previously queued with queue().
#     e.g. music.play('handel')
# music.play_once(name)
#     Similar to play(), but the music will stop after playing through once.
# music.queue(name)
#     Similar to play_once(), but instead of stopping the current music, 
#     the track will be queued to play after the current track finishes 
#     (or after any other previously queued tracks).
# music.stop()
#     Stop the music.
# music.pause()
#     Pause the music temporarily. It can be resumed by calling unpause().
# music.unpause()
#     Unpause the music.
# music.is_playing()
#     Returns True if the music is playing (and is not paused), False otherwise.
# music.fadeout(duration)
#     Fade out and eventually stop the current music playback.
#     Parameters:	duration – in the duration seconds over which the sound will be faded out. 
#     For example, to fade out over half a second, call music.fadeout(0.5).
# music.set_volume(volume)
#     Set the volume of the music system.
#     This takes a number between 0 (meaning silent) and 1 (meaning full volume).
# music.get_volume()
#     Get the current volume of the music system.
# on_music_end():
#     Called when a music track finishes.
#     Note: this only works when play musi using music.play_once()
#     it will not be called if the track is configured to loop


# Clock object ==========================================================================
# clock object will be used when you want to schedule  game event to occur at a later time.
# For example, we may want a big boss alien to appear after 60 seconds. 
# class Clock:
#     schedule(callback, delay)
#         Schedule callback to be called after the given delay.
#         Repeated calls will schedule the callback repeatedly.
#         Parameters:	
#             callback – A callable that takes no arguments.
#             delay – The delay, in seconds, before the function should be called.
#         e.g.
#             def fire_laser():
#                 lasers.append(player.pos)
#             def on_mouse_down():
#                 clock.schedule(fire_laser, 1.0)
#     schedule_interval(callback, interval)
#         Schedule callback to be called repeatedly.
#         Parameters:	
#             interval – The interval in seconds between calls to callback.
#     unschedule(callback)
#         Unschedule callback if it has been previously scheduled, either because it
#         has been scheduled with schedule() and has not yet been called, or because
#         it has been scheduled to repeat with schedule_interval().
#     schedule_unique(callback, delay)
#         Schedule callback to be called once after the given delay.
#         If callback was already scheduled, cancel and reschedule it. 
#         if scheduled multiple times,after calling schedule_unique,it will be 
#         scheduled exactly only once.




# Animation object ==========================================================================
# animate(object, tween='linear', duration=1, on_finished=None, **targets)
# Animate the attributes on object from their current value to value specified in 
# the targets keywords.
# Parameters:	
#     tween – The type of tweening to use.
#     duration – The duration of the animation, in seconds.
#     on_finished – Function called when the animation finishes.
#     targets – The target values for the attributes to animate.

# The tween argument can be one of the following:
#     ‘linear’ 	            Animate at a constant speed from start to finish
#     ‘accelerate’ 	        Start slower and accelerate to finish
#     ‘decelerate’ 	        Start fast and decelerate to finish
#     ‘accel_decel’ 	    Accelerate to mid point and decelerate to finish
#     ‘end_elastic’ 	    Give a little wobble at the end
#     ‘start_elastic’ 	    Have a little wobble at the start
#     ‘both_elastic’ 	    Have a wobble at both ends
#     ‘bounce_end’ 	        Accelerate to the finish and bounce there
#     ‘bounce_start’ 	    Bounce at the start
#     ‘bounce_start_end’ 	Bounce at both ends






pgzrun.go()

