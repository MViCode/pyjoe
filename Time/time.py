import time
import calendar

# Time ==================================================================
# dates before the epoch cannot be represented in this form.
# Dates in the far future also cannot be represented this way
# the cutoff point is sometime in 2038 for UNIX and Windows.
ticks = time.time()
print("Since 12:00am Jan1,1970(Epoch) =", ticks)

second1 = time.perf_counter()

time.sleep(1.5)

print("CPU time Interval: =", time.perf_counter()-second1)

'''
Time Tuple:
    index 	Field 	        Values
    0       4-digit year 	2008
    1       Month 	        1 to 12
    2 	    Day 	        1 to 31
    3 	    Hour 	        0 to 23
    4 	    Minute 	        0 to 59
    5 	    Second 	        0 to 61 (60 or 61 are leap-seconds)
    6 	    Day of Week 	0 to 6 (0 is Monday)
    7 	    Day of year 	1 to 366 (Julian day)
    8 	    Daylight savings 	-1, 0, 1, -1 means library determines DST
'''
timetuple = time.localtime(time.time())
print("timetuple is =", timetuple)

# readable time format
localtime = time.asctime(time.localtime(time.time()))
print("localtime is =", localtime)

# Calenda ==================================================================
cal = calendar.month(2008, 1)
print("Here is the calendar:")
print(cal)

print(calendar.isleap())

