import math
import time

'''
DEFINITION of Prime Number
    - A prime number has to be a positive integer
    - Divisible by exactly 2 integers (1 and itself)
    - 1 is not a prime number
'''
def IsPrime1(Num):
    IsPrimeFlag = True          # Assume the number is prime in very begining
    for i in range(2,Num):      # Check if the number can be divided w/o remainder by number 2 to Num-1 
        if Num % i == 0:
            IsPrimeFlag = False
            break      # will be more efficient, as long as confirmed is not prime, will stop
    return IsPrimeFlag          # return result


def IsPrime2(Num):
    IsPrimeFlag = True          # Assume the number is prime in very begining
    for i in range(2,Num//2+1):    # if a Num has prime divisor, then it will <=Num//2
        if Num % i == 0:
            IsPrimeFlag = False
            break
    return IsPrimeFlag          # return result


def IsPrime3(Num):
    IsPrimeFlag = True           # Assume the number is prime in very begining
    if (Num!=2) and (Num%2==0):  # check if the number can be divided by 2 w/o remainder(incase it isn't 2)
        IsPrimeFlag = False
    else:
        for i in range(3,Num//2+1,2):  # Except 2, the prime divisor will be a odd number in range of 3 to <=Num//2
            if Num % i == 0:
                IsPrimeFlag = False
                break
    return IsPrimeFlag          # return result

def IsPrime4(Num):
    IsPrimeFlag = True           # Assume the number is prime in very begining
    if (Num!=2) and (Num%2==0):  # check if the number can be divided by 2 w/o remainder(incase it isn't 2)
        IsPrimeFlag = False
    else:
        for i in range(3,int(math.sqrt(Num))+1,2):    # factors will be pair, e.g. 1*100,2*50, 4*25 ...10*10,so just need check to sqrt(Num)
        #for i in range(3,int(Num**0.5)+1,2):
            if Num % i == 0:
                IsPrimeFlag = False
                break
    return IsPrimeFlag          # return result
#其实，只要尝试小于 √x 的【质数】即可。可把已经算出的质数先保存起来，然后用于后续的试除，提高效率

def FilterPrime(Num):  #Filter
    PrimeList=[0]*(Num+1)   # all Prime number labels are set to 0
    
    for i in range(2, len(PrimeList)):   # starting from 2 as 0,1 is spdecial
        if i==2 or i&1==True:
            PrimeList[i]=1  # 2 & odd number label set to 1
        else:
            PrimeList[i]=0  # even number label set to 0

    for i in range(3, len(PrimeList),2): #Search all odd number
        if PrimeList[i] == 1:  # if prime number label is 1
            if IsPrime4(i)== True:  # if i is prime, then all its times are not prime numbers
                for j in range(i+i, len(PrimeList), i): # for all i*1,2,3...
                    PrimeList[j]=0
            else:
                PrimeList[i]=0
        else:
            pass
    
    PrimeCount = 0
    for i in range(2, len(PrimeList)):
        if PrimeList[i]==1:
            PrimeCount += 1
            if PrimeCount % 10 !=0:
                print("{0:8d}".format(i),end='')
            else:
                print("{0:8d}".format(i))
    print("\n--- Total Prime <=",Num,"is:",PrimeCount," ---")


def main():  # Define the main function
    Num=int(input("Input the number:"))
    #FilterPrime(Num)

    PrimeCount = 0    # Count the total number of Prime number in range of 0 to Num
    for i in range(2,Num+1):  # Check if each number is prime number
        if IsPrime4(i) == True:   # if i is prime number, then print it out and add count
            PrimeCount += 1
            if PrimeCount % 10 !=0:
                print("{0:8d}".format(i),end='')
            else:
                print("{0:8d}".format(i))
    print("\n --- Total Prime <=",Num,"is:",PrimeCount," ---")

if __name__== "__main__":   #invoke main function
    start_time = time.time()
    main()
    print("--- Program used %.3fs seconds ---" % (time.time() - start_time))