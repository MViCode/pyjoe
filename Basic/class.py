class Student:
    # class variable,shared among all instances of class
    # accessed as Student.empCount from inside or outside the class
    studentCount = 0 

    def __init__(self, name, score): # Constructor 
        self.name = name
        self.score = score
        Student.studentCount += 1
    
    def __del__(self):              # Deconstructor
        print(self.__class__.__name__ + "destroyed")
   
    # declare other class methods like normal functions with the exception that
    # the first argument of method is self. 
    # Python adds the self argument to the list for you; 
    # you do not need to include it when you call the methods.
    def displayCount(self):
        print("Total Student %d" % Student.studentCount)

    def displayStudent(self):
        print ("Name : ", self.name,  ", Score: ", self.score)

joe=Student('Joe',100)
Kate=Student("Kate",60)

joe.displayStudent()
Kate.displayStudent()
print("total Student %d"%Student.studentCount)

print("Student.__doc__:", Student.__doc__)
print("Student.__name__:", Student.__name__)
print("Student.__module__:", Student.__module__)
print("Student.__bases__:", Student.__bases__)
print("Student.__dict__:", Student.__dict__)

class Parent:        # define parent class
    parentAttr = 100
    def __init__(self):
        print("Calling parent constructor")

    def parentMethod(self):
        print('Calling parent method')

    def myMethod(self):
        print('Calling parent mymethod')

    def setAttr(self, attr):
        Parent.parentAttr = attr
    def getAttr(self):
        print("Parent attribute :", Parent.parentAttr)

class Child(Parent): # define child class
    def __init__(self):
        print("Calling child constructor")

    def childMethod(self):
        print('Calling child method')

    def myMethod(self):
        print('Calling Child mymethod')

c = Child()          # instance of child
c.childMethod()      # child calls its method
c.parentMethod()     # calls parent's method
c.myMethod()
c.setAttr(200)       # again call parent's method
c.getAttr()          # again call parent's method