import math
import time

'''
Rules for Python variables:
    A variable name must start with a letter or the underscore character
    A variable name cannot start with a number
    A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
    Variable names are case-sensitive (age, Age and AGE are three different variables)
'''

'''
There are three numeric types in Python:
    int
    float
    complex
Variables of numeric types are created when you assign a value to them:
'''
def NumericTypes():
    Age = 9    #int
    Cash = 20.5 #float
    frq=3+4j    #complex
    print(type(Age))
    print(type(Cash))
    print(type(frq))

def Strings():
    print("Please input a string:")
    TestStr=input() # ask the user for input

    Functions = ["1=trip String","2=String length","3=Upper case","4=Lower Case","5=Replace","6=Split String","7=Exit"]
    while(1):
        print("\nAvailable Functions:")
        for strBox in Functions:
            print(strBox)
        try:
            opt=int(input("Please select Functions:")) # ask the user for input
            if opt==1:
                print("Result:", TestStr.strip()) #strip() method removes any whitespace from the beginning or the end
            elif opt==2:
                print("Result:",len(TestStr))
            elif opt==3:
                print("Result:",TestStr.lower()) #returns the string in lower case
            elif opt==4:
                print("Result:",TestStr.upper()) #returns the string in upper case
            elif opt==5:
                key=input("Please input string to be replaced:")
                by=input("Please input string you want to use:")
                print("Result:",TestStr.replace(key,by)) #replaces a string with another string
            elif opt==6:
                key=input("Please input splitor:")
                print("Result:",TestStr.split(",")) #splits the string into substrings if it finds instances of the separator
            elif opt==7:
                break
            else:
                print("Not a valid input")
            co=input("Do you want to continue?(Y/N)")
            if co=='y' or co=='yes':
                pass
            else:
                break
        except ValueError:
            print("Please input a Number!")

def ListFunctions():
    '''
    Four collection data types in the Python programming language:
        Type        Ordered?        Changeable?            Duplicate members?
        -List       Yes             Yes                    Yes
        -Tuple      Yes             No(not add/remove)     Yes
        -Set        No(can seach)   No(allow add/Remove)   No
        -Dictionary No(can seach)   Yes(allow add/Remove)  No
    '''
    List = ["apple","peach","mongo"]    # List = [1,2,3,4]
    for box in List:   # loop through the list
        print(box)
    
    Functions = ["0=Exit","1=Access Items","2=exists?","3=Append","4=Insert","5=Remove",\
        "6=Pop","7=Delete","8=clear","9=copy","10=Count","11=Reverse","12=sort","13=Extend"]
    while(1):
        print("\nAvailable Functions:")
        for strBox in Functions:
            print(strBox)
        try:
            opt=int(input("Please select Functions:")) # ask the user for input
            if opt==0:
                break
            elif opt==1:
                try:
                    item=int(input("Please input item index to PRINT:")) # ask the user for input
                    print("Result of item"+str(item)+" is:", List[item]) #strip() method removes any whitespace from the beginning or the end
                except ValueError:
                    print("Invilad input!")
            elif opt==2:
                item=input("Please input item to check EXISTS:")
                if item in List:    #check if exists
                    print("Yes,", item, "is in the list.")
            elif opt==3:
                item=input("Please input item to APPEND:")
                List.append(item)
                print(List)
            elif opt==4:
                item=input("Please input item to INSERT:")
                index=int(input("Please input id to INSERT:"))
                List.insert(index,item)
                print(List)            
            elif opt==5:
                item=input("Please input item to REMOVE:")
                List.remove(item)
                print(List)            
            elif opt==6: 
                index=int(input("Please input index to POP:"))
                List.pop(index) # pop removes the specified index,or the last item if index is not specified
                print(List)            
            elif opt==7: 
                index=int(input("Please input index to DEL:"))
                del List[index] # del List
                print(List)            
            elif opt==8: 
                List.clear() #empties the list
                print(List)            
            elif opt==9: 
                newList=List.copy() #empties the list
                print("new list is:",newList)            
            elif opt==10:
                item=int(input("Please input item to COUNT:"))
                List.count(item) #Returns the number of elements with the specified value
            elif opt==11: 
                List.reverse() 
                print(List)            
            elif opt==12: 
                List.sort() 
                print(List)            
            elif opt==13: 
                List.extend(List) 
                print(List)            
            else:
                print("Not a valid input")
            co=input("Do you want to continue?(y)")
            if co=='y' or co=='yes':
                pass
            else:
                break
        except ValueError:
            print("Please input a Number!")
'''
    List Methods Description
    append()	Adds an element at the end of the list
    clear()	    Removes all the elements from the list
    copy()	    Returns a copy of the list
    count()	    Returns the number of elements with the specified value
    extend()	Add the elements of a list (or any iterable), to the end of the current list
    index()	    Returns the index of the first element with the specified value
    insert()	Adds an element at the specified position
    pop()	    Removes the element at the specified position
    remove()	Removes the item with the specified value
    reverse()	Reverses the order of the list
    sort()	    Sorts the list
'''

def TupleFunctions():
    Tuples = ("apple","peach","mongo")    # Tuples = (1,2,3,4)
    for box in Tuples:   # loop through the list
        print(box)
    
    Functions = ["0=Exit","1=Access Items","2=exists?","3=Index","4=Count"]
    while(1):
        print("\nAvailable Functions:")
        for strBox in Functions:
            print(strBox)
        try:
            opt=int(input("Please select Functions:")) # ask the user for input
            if opt==0:
                break
            elif opt==1:
                try:
                    item=int(input("Please input item index to PRINT:")) # ask the user for input
                    print("Result of item"+str(item)+" is:", Tuples[item]) #strip() method removes any whitespace from the beginning or the end
                except ValueError:
                    print("Invilad input!")
            elif opt==2:
                item=input("Please input item to check EXISTS:")
                if item in Tuples:    #check if exists
                    print("Yes,", item, "is in the list.")
            elif opt==3: #Searches the tuple for a specified value and returns the position
                item=input("Please input item to Search:")
                print("Index of item is:",Tuples.index(item))                     
            elif opt==4:
                item=int(input("Please input item to COUNT:"))
                Tuples.count(item) #Returns the number of elements with the specified value
          
            else:
                print("Not a valid input")
            co=input("Do you want to continue?(y)")
            if co=='y' or co=='yes':
                pass
            else:
                break
        except ValueError:
            print("Please input a Number!")

def SetFunctions():
    Set = {"apple","peach","mongo"}
    for box in Set:   # loop through the list
        print(box)
    
    Functions = ["0=Exit","1=Access Items","2=exists?","3=Add","4=Update","5=remove" \
        "6=Discard","7=pop","8=Clear","9=Del"]
    while(1):
        print("\nAvailable Functions:")
        for strBox in Functions:
            print(strBox)
        try:
            opt=int(input("Please select Functions:")) # ask the user for input
            if opt==0:
                break
            elif opt==1:
                try:
                    item=int(input("Please input item index to PRINT:")) # ask the user for input
                    print("Result of item"+str(item)+" is:", Set[item]) #strip() method removes any whitespace from the beginning or the end
                except ValueError:
                    print("Invilad input!")
            elif opt==2:
                item=input("Please input item to check EXISTS:")
                if item in Set:    #check if exists
                    print("Yes,", item, "is in the list.")
            elif opt==3:
                item=input("Please input item to ADD:")
                Set.add(item)
                print(Set)
            elif opt==4: 
                print("['Grape','Watermelon','Organe'] will be add to Set")
                Set.update(["Grape","Watermelon","Organe"])
                print(Set)
            elif opt==5:
                item=input("Please input item to remove:")
                Set.remove(item)
                print(Set)
            elif opt==6:
                print("['Grape','Watermelon','Organe'] will be discard from Set")
                Set.discard(["Grape","Watermelon","Organe"])
                print(Set)                     
            elif opt==7:
                Set.pop() #Remove the last item by using the pop()
                print(Set) 
            elif opt==8:
                Set.clear() #Empty the set
                print(Set) 
            elif opt==9:
                del Set
                print(Set) 
            else:
                print("Not a valid input")
            co=input("Do you want to continue?(y)")
            if co=='y' or co=='yes':
                pass
            else:
                break
        except ValueError:
            print("Please input a Number!")
        '''
        Set Method 	Description
        add()	                Adds an element to the set
        clear()	                Removes all the elements from the set
        copy()	                Returns a copy of the set
        difference()	        Returns a set containing the difference between two or more sets
        difference_update()	    Removes the items in this set that are also included in another specified set
        discard()	            Remove the specified item
        intersection()	        Returns a set, that is the intersection of two other sets
        intersection_update()	Removes the items in this set that are not present in other, specified set(s)
        isdisjoint()	        Returns whether two sets have a intersection or not
        issubset()	            Returns whether another set contains this set or not
        issuperset()	        Returns whether this set contains another set or not
        pop()	                Removes an element from the set
        remove()	            Removes the specified element
        symmetric_difference()	Returns a set with the symmetric differences of two sets
        symmetric_difference_update()	inserts the symmetric differences from this set and another
        union()	                Return a set containing the union of sets
        update()	            Update the set with the union of this set and others
        '''
def Dictionary():
    Dic = {
        "Class": "SPJ-3",
        "Grade": 3,
        "Updated Date": "Mar19,2019",
        "Joe":100,
        "Mike":50,
        "Dan":0}
    print(Dic)
    
    Functions = ["0=Exit","1=List all Keys","2=List all values","3=List all Key & Values","4=Exists","5=Add" \
        "6=pop w/ Key","7=pop item","8=Del w/ Key","9=Delete Dic","10=Clear"]
    while(1):
        print("\nAvailable Functions:")
        for strBox in Functions:
            print(strBox)
        try:
            opt=int(input("Please select Functions:")) # ask the user for input
            if opt==0:
                break
            elif opt==1:
                for key in Dic:   
                    print(key)
            elif opt==2:
                for key in Dic:   
                    print(Dic[key])
                #for value in Dic.values:   
                #    print(value)    
            elif opt==3:
                for key,value in Dic.items():   
                    print(key,value)
            elif opt==4:
                key=input("Please input key to check:")
                if key in Dic:
                print("yes, the key ", Set,"is in Dictionary")
            elif opt==5:
                Key=input("Please input key to Add:")
                Value=input("Please input key value to Add:")
                Dic[Key]=Value
                print(Dic)                     
            elif opt==6:
                Key=input("Please input key to pop:")
                Dic.pop(key) #removes the item with the specified key name
                print(Dic) 
            elif opt==7:
                Dic.pop() # removes the last inserted item (in versions before 3.7, a random item is removed instead)
                print(Dic) 
            elif opt==8:
                key=input("Please input key to Delete:")
                del Dic[key]
                print(Dic) 
            elif opt==9:
                del Dic
                print(Dic) 
            elif opt==10:
                Dic.clear()
                print(Dic) 
            else:
                print("Not a valid input")
            co=input("Do you want to continue?(y)")
            if co=='y' or co=='yes':
                pass
            else:
                break
        except ValueError:
            print("Please input a Number!")
'''
Method 	            Description
clear()	            Removes all the elements from the dictionary
copy()	            Returns a copy of the dictionary
fromkeys()	        Returns a dictionary with the specified keys and values
get()	            Returns the value of the specified key
items()	            Returns a list containing the a tuple for each key value pair
keys()	            Returns a list containing the dictionary's keys
pop()	            Removes the element with the specified key
popitem()	        Removes the last inserted key-value pair
setdefault()	    Returns the value of the specified key. If the key does not exist: insert the key, with the specified value
update()	        Updates the dictionary with the specified key-value pairs
values()	        Returns a list of all the values in the dictionary
'''

def main():  # Define the main function
    # StringFunctions()
    # TupleFunctions()
    # ListFunctions()
    # SetFunctions()
     Dictionary()
    
    

if __name__== "__main__":   #invoke main function
    main()

