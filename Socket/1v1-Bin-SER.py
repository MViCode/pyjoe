import socket
import sys

socketObj = socket.socket()
socketObj.bind(("localhost",59999))

# allow up to 10 un-accept()ed incoming TCP connections to be queued up by the TCP stack at the same time". 
# If 10 incoming TCP connections are already in the TCP stack's queue for this socket, 
# any additional ones will be refused (until the program drains the queue by accept()ing some of the connections in it) 
socketObj.listen(10) 
print("Waiting Client..")

i = 1
stop='n'
while (stop=='n'):
    conn, address = socketObj.accept()
    print(address)

    fl = open('file_'+ str(i)+".png",'wb') #open in binary
    i=i+1
    tmp=conn.recv(1024)
    if tmp:
        print('Receiving File..',end='')
        while tmp:
            print('.',end='')
            fl.write(tmp)
            # recibimos y econnribimos en el fichero
            tmp = conn.recv(1024)
        print("\nFile Received.")
    else:
        print("No Data Received!")
    fl.close()
    conn.close()
    stop=input("Stop Server?(Y/N)")
  

socketObj.close()