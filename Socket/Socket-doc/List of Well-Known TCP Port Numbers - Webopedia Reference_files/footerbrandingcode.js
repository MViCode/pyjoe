var developerProperty = "A Developer.com Property";
var eweekProperty = "An eWEEK Property";
var logoPath = "/icom_includes/footers/img/";

var siteFooterConfig = {
    "antionline": {footercopyright: eweekProperty, image: "antilogo.gif", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: ""},
    "baselinemag": {footercopyright: eweekProperty, image: "baselinemag.png", commonfooteraboutuslink: "/about-us.html", commonsitemaplink: "/sitemap.html"},
    "channelinsider": {footercopyright: eweekProperty, image: "cioinsider.png", commonfooteraboutuslink: "/about-us.html", commonsitemaplink: "/sitemap.html"},
    "cioinsight": {footercopyright: eweekProperty, image: "cioinsight.png", commonfooteraboutuslink: "/about-us.html", commonsitemaplink: "/sitemap.html"},
    "codeguru": {footercopyright: developerProperty, image: "codeguru.gif", commonfooteraboutuslink: "/about.php", commonsitemaplink: "/sitemap/"},
    "databasejournal": {footercopyright: eweekProperty, image: "dbjlogo.gif", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/", commonsitemaplink: "/sitemap.php"},
    "dbasupport": {footercopyright: developerProperty, image: "dbasupport.gif", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: ""},
    "developer": {footercopyright: developerProperty, image: "developer_logo.jpg", commonfooteraboutuslink: "/about.html", commonsitemaplink: "/sitemap.html"},
    "devx": {footercopyright: developerProperty, image: "devx.gif", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "enterpriseappstoday": {footercopyright: eweekProperty, image: "EnterpriseAppsToday.gif", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "enterprisemobiletoday": {footercopyright: eweekProperty, image: "enterprisemobiletoday.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "enterprisenetworkingplanet": {footercopyright: eweekProperty, image: "enp.gif", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "eseminarslive": {footercopyright: eweekProperty, image: "eseminarslive.png", commonfooteraboutuslink: "/c/s/About-Us/ ", commonsitemaplink: "/"},
    "flashkit": {footercopyright: developerProperty, image: "flashkit.png", commonfooteraboutuslink: "/about.php", commonsitemaplink: "/sitemap.html"},
    "freevbcode": {footercopyright: developerProperty, image: "freevbcode.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "htmlgoodies": {footercopyright: developerProperty, image: "htmlgoodies.png", commonfooteraboutuslink: "/introduction/about/", commonsitemaplink: "/sitemap.html"},
    "infostor": {footercopyright: eweekProperty, image: "infostor.png", commonfooteraboutuslink: "/index/about-us.html", commonsitemaplink: "/index/site-map.html"},
    "internetnews": {footercopyright: eweekProperty, image: "internetnews.jpg", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "itbusinessedge": {footercopyright: eweekProperty, image: "itbe.gif", commonfooteraboutuslink: "/about", commonsitemaplink: "/sitemap/"},
    "itchannelplanet": {footercopyright: eweekProperty, image: "itchannelplanet.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "itsmwatch": {footercopyright: eweekProperty, image: "itsmwatch.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "jguru": {footercopyright: developerProperty, image: "jguru.png", commonfooteraboutuslink: "/about", commonsitemaplink: "/sitemap.html"},
    "linux-mag": {footercopyright: eweekProperty, image: "lmlogo.jpg", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap/"},
    "linuxplanet": {footercopyright: eweekProperty, image: "linuxplanet.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/sitemap.html"},
    "linuxtoday": {footercopyright: eweekProperty, image: "linuxtoday.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/", commonsitemaplink: "/sitemap.html"},
    "serverwatch": {footercopyright: eweekProperty, image: "serverwatch.png", commonfooteraboutuslink: "/about/", commonsitemaplink: "/sitemap/"},
    "smallbusinesscomputing": {footercopyright: eweekProperty, image: "smallbusinesscomputing.png", commonfooteraboutuslink: "/about-us.html", commonsitemaplink: "/sitemap.html"},
    "sqlcourse": {footercopyright: eweekProperty, image: "sqlcourse.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com", commonsitemaplink: "/sitemap.html"},
    "sqlcourse2": {footercopyright: eweekProperty, image: "sqlcourse2.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/", commonsitemaplink: "/sitemap.html"},
    "sysopt": {footercopyright: eweekProperty, image: "sysopt.gif", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/"},
    "vbforums": {footercopyright: developerProperty, image: "vbforums.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: ""},
    "webopedia": {footercopyright: eweekProperty, image: "webopedia.png", commonfooteraboutuslink: "/AboutUs.asp", commonsitemaplink: "/sitemap.html"},
    "4guysfromrolla": {footercopyright: developerProperty, image: "4guysfromrolla.png", commonfooteraboutuslink: "https://www.quinstreetb2btech.com/about-us/", commonsitemaplink: "/"},
    "esecurityplanet": {footercopyright: eweekProperty, image: "esecurityplanet.png", commonfooteraboutuslink: "/about", commonsitemaplink: "/sitemap.html"},
    "datamation": {footercopyright: eweekProperty, image: "datamation.png", commonfooteraboutuslink: "/about", commonsitemaplink: "/sitemap.html"},
    "enterprisestorageforum": {footercopyright: eweekProperty, image: "enterprisestorageforum.png", commonfooteraboutuslink: "/about", commonsitemaplink: "/sitemap.html"},
};
var currentSiteName = window.location.hostname;
var siteNameArray = currentSiteName.split(".");
var siteName = getFullSiteName(siteNameArray);

document.getElementById("commonsitemaplink").href = siteFooterConfig[siteName].commonsitemaplink;
document.getElementById("commonfooteraboutuslink").href = siteFooterConfig[siteName].commonfooteraboutuslink;
document.getElementById("commonfooterlogo").src = logoPath + siteFooterConfig[siteName].image;
document.getElementById("commonfooterpropertytext").innerHTML = siteFooterConfig[siteName].footercopyright;
document.getElementById("commonfootercopyright").innerHTML = "Copyright " + new Date().getFullYear() + " QuinStreet Inc. All Rights Reserved.";
if (siteName == "codeguru") {
    if (currentSiteName.indexOf("mobile.") !== -1 || currentSiteName.indexOf("forums.") !== -1) {
        document.getElementById("commonfooterlogo").src = logoPath + "codeguruMobile.png";
    }
}
if (currentSiteName.indexOf("mobile.") !== -1) { // Checking if site is mobile.sitename.com
    document.getElementById("footerlinks").style.display = "none";
    document.getElementById("commonfooterlogo").style.paddingTop = "10px";

}

if (currentSiteName.indexOf("www.antionline.com") !== -1 || currentSiteName.indexOf("www.dbasupport.com") !== -1 || currentSiteName.indexOf("forums.codeguru.com") !== -1 || currentSiteName.indexOf("forums.databasejournal.com") !== -1 || currentSiteName.indexOf("forums.devx.com") !== -1 || currentSiteName.indexOf("forums.smallbusinesscomputing.com") !== -1 || currentSiteName.indexOf("ww.sysopt.com") !== -1 || currentSiteName.indexOf("www.vbforums.com") !== -1 || currentSiteName.indexOf("board.flashkit.com") !== -1) {
    document.getElementById("commonfootercontactuslink").href = "/contact-us.php";
    document.getElementById("commonfooteraboutuslink").href = "https://www.quinstreetb2btech.com/about-us/";
    document.getElementById("commonadvertiselink").href = "https://www.quinstreetb2btech.com/contact/";
    document.getElementById("acceptableusepolicy").style.display = "block";
    document.getElementById("acceptableusepolicy").style.marginBottom = "5px";
    document.getElementById("acceptableusepolicy").style.marginTop = "5px";
    document.getElementById("commonsitemaplink").style.display = "none";
}

function getFullSiteName(siteArr) {
    for (var i = 0; i <= siteArr.length; i++) {
        if (siteArr[i] === "com") {
            return siteArr[i - 1];
        }
    }
}if(siteFooterConfig[siteName].footercopyright === developerProperty ){
    var idExists = document.getElementById("Developer.com-NonACLHeader");
    if(idExists)
        idExists.style.display = "block";
}
else{
    var idExists = document.getElementById("EWEEK-NonACLHeader");
    if(idExists)
        idExists.style.display = "block";
}