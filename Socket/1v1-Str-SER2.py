import socket
import time

def Main():
    host = "" # accept everyone
    port = 65000

    mySocket = socket.socket()  # Create Socket Object
    mySocket.bind((host,port))  # bind the endpoint(host+port pair)
    mySocket.listen(5)          # Server start listen, parameter indicates the maximum number of queued connections

    ''' returns (conn, address) pair where ‘conn’ is new socket object used to send and receive 
    data on the communication channel, and ‘address’ is the IP address tied to the socket on 
    another end of the channel.'''
    conn, clientaddr = mySocket.accept()
    print ("Connection from: " + str(clientaddr))
    
    message = 'go'
    while message != 'q':
        '''
        Since Python 3.0, strings are stored as Unicode, i.e. each character in the string is represented by a code point. So, each string is just a sequence of Unicode code points.
        For efficient storage of these strings, the sequence of code points are converted into set of bytes. The process is known as encoding.
        There are various encodings present which treats a string differently. The popular encodings being utf-8, ascii, etc.
        Using string's encode() method, you can convert unicoded strings into any encodings supported by Python. By default, Python uses utf-8 encoding.
        '''
        data = conn.recv(1024).decode()
        if not data:
            break
        print ("Received from Client: " + str(data))
        message = input("Server(q=Exit)?: ")
        conn.send(message.encode())
                                                
    conn.close()
    mySocket.close()            
if __name__ == '__main__':
    Main()