#Server for multithreaded (asynchronous) chat application.
from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread

def accept_incoming_connections():
    #Sets up handling for incoming CONNs.
    while True:
        conn, conn_address = SERVER.accept()
        print("%s:%s has connected." % conn_address)
        conn.send(bytes("Greetings from the cave! Now type your name and press enter!", "utf8"))
        addresses[conn] = conn_address
        Thread(target=handle_conn, args=(conn,)).start()

def handle_conn(conn):  # Takes conn socket as argument.
    #Handles a single conn connection.
    name = conn.recv(BUFSIZ).decode("utf8")
    welcome = 'Welcome %s! If you want to quit, type {quit} to exit.' % name
    conn.send(bytes(welcome, "utf8"))
    msg = "%s has joined the chat!" % name
    broadcast(bytes(msg, "utf8"))
    CONNs[conn] = name
    while True:
        msg = conn.recv(BUFSIZ)
        if msg != bytes("{quit}", "utf8"):
            broadcast(msg, name+": ")
        else:
            conn.send(bytes("{quit}", "utf8"))
            conn.close()
            del CONNs[conn]
            broadcast(bytes("%s has left the chat." % name, "utf8"))
            break

def broadcast(msg, prefix=""):  # prefix is for name identification.
    #Broadcasts a message to all the CONNs.
    for sock in CONNs:
        sock.send(bytes(prefix, "utf8")+msg)

        
CONNs = {}
addresses = {}

HOST = ''
PORT = 33000
BUFSIZ = 1024
ADDR = (HOST, PORT)

SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDR)

if __name__ == "__main__":
    SERVER.listen(5)
    print("Waiting for connection...")
    ACCEPT_THREAD = Thread(target=accept_incoming_connections)
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join() # ensure mainloop waits for thread to complete.
SERVER.close()