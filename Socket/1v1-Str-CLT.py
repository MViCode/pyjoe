import socket

def Main():
    host = '127.0.0.1'
    port = 65000

    mySocket = socket.socket()
    mySocket.connect((host,port))

    message = input(" Client(q=Exit)?: ")
    while message != 'q':
        mySocket.send(message.encode())
        data = mySocket.recv(1024)
        print ('Server: ' + data)
        message = input("Client(q=Exit)?:")

    mySocket.close()

if __name__ == '__main__':
    Main()