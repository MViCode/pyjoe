import socket
import sys

socketObj = socket.socket()
socketObj.connect(("localhost",59999))

fl = open ("test.png", "rb")
buf = fl.read(1024)
print('Sending File..')
if buf:
    while buf:
        print('.',end='')
        socketObj.send(buf)
        buf = fl.read(1024)
    print("\nFile sent.")
else:
    print("not a valid file!")
socketObj.close()