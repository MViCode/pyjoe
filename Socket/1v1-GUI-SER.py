from tkinter import *
from tkinter import ttk
import socket
from threading import Thread

def InsertMsg(msg='',align='',textcolor='red'):
    #Enable the text before insert text
    msgWin.configure(state='normal')
    # def alignment style
    msgWin.tag_configure('tag-left', justify='left',foreground=textcolor)
    msgWin.tag_configure('tag-center', justify='center',foreground=textcolor)
    msgWin.tag_configure('tag-right', justify='right',foreground=textcolor)
    if align=='center':
        msgWin.insert(END,msg+'\n','tag-center')
    elif align=='right':
        msgWin.insert(END,msg+'\n','tag-right')
    else:
        msgWin.insert(END,msg+'\n','tag-left')
    msgWin.see(END) # scrool to the bottom
    #Diable the Text after insert
    msgWin.configure(state='disable') #Enable the text before insert text
    
# Def GUI functions ==============================================
def OnSend(event=None):
    InsertMsg(msg.get()+':SERVER','right','green')
    # Send message to client side
    CONNs['conn'].send(bytes(msg.get(), "utf8"))
    # Clear entry box
    msg.delete(0,last=END) 

# Def Socket Thread functions ====================================
def AcceptConnection():  # Takes conn socket as argument.
    conn, conn_address = SER_SocketObj.accept()
    CONNs['conn']=conn
    InsertMsg('%s:%s has connected.'% conn_address,'center','red')
    
    #send message to client
    conn.send(bytes("Hi,Greetings from the cave!", "utf8"))
    #receive message from client
    while True:
        ClientMsg = conn.recv(1024).decode("utf8")
        if ClientMsg != "q!":
            InsertMsg('CLIENT:'+ClientMsg,'left','blue')
        else:
            # send quit and close the window.
            conn.send(bytes("q!", "utf8"))
            conn.send(bytes("SERVER has left!", "utf8"))
            conn.close()
            root.quit()
            break

# Create main GUI in memory ======================================
root = Tk()
#root.geometry("800x400")
frame=ttk.Labelframe(root,text="SERVER")
# configure the rows and columns weight to non-zero so that they will take up the extra space
Grid.rowconfigure(root, 0, weight=1)
Grid.columnconfigure(root, 0, weight=1)
frame.grid(row=0,column=0,sticky=('nswe'))

msgWin = Text(frame,wrap="word", state='normal') 
msgWin.grid(row=0,column=0,columnspan=10,sticky=("nsew"))

# Add Horizontal Scrollbar
#ScrX = ttk.Scrollbar(frame, orient=HORIZONTAL, command=msgWin.xview)
#ScrX.grid(row=6,column=1,columnspan=5,sticky=(W,E))
# Add Vertical Scrollbar
ScrY = ttk.Scrollbar(frame, orient=VERTICAL, command=msgWin.yview)
ScrY.grid(row=0,column=10,rowspan=10,sticky=(N,S))
# Link the scroolbars with Widget
msgWin.configure(yscrollcommand=ScrY.set)

# Add a ebtry box
msg = ttk.Entry(frame)
msg.bind("<Return>", OnSend)
msg.grid(row=13,column=1,columnspan=8,sticky=("nsew"))
 
BtnHello = ttk.Button(frame, text = "Send>",  command = OnSend)
BtnHello.grid(row=13,column=9)

# Server Socket =================================================
CONNs={}
HOST='127.0.0.1'    # Server IP
PORT=59000          # Server Port
SER_SocketObj = socket.socket()  # Create Socket Object
SER_SocketObj.bind((HOST,PORT))  # bind the endpoint(host+port pair)
InsertMsg('SERVER is up, waiting client...','center','red')
SER_SocketObj.listen(5) # Server start listen, the maximum number of queued connections is 5

SER_Thread = Thread(target=AcceptConnection)
SER_Thread.start() 
#SER_Thread.join() # ensure mainloop waits for thread to complete.
# Run main loop and show the window

root.mainloop()
SER_SocketObj.close()