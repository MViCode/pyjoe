#Script for Tkinter GUI chat client.
from tkinter import *
from tkinter import ttk
import socket
from threading import Thread

# Def GUI functions ==============================================
def OnSend(event=None):
    msgWin.config(fg='red')
    msgWin.insert(END,'CLIENT:'+msg.get()+'\n',"font")
    msgWin.see(END)
    CLT_SocketObj.send(bytes(msg.get(), "utf8"))
    msg.delete(0,last=END)
    if msg.get() == "q!":
        CLT_SocketObj.close()
        root.quit()

# Def Socket Thread functions ====================================
def OnReceive():  # Takes conn socket as argument.
    #Handles a single conn connection.
    while True:
        try:
            ClientMsg = CLT_SocketObj.recv(1024).decode("utf8")
            msgWin.insert(END,'SERVER:'+ClientMsg+'\n',"font")
            msgWin.see(END)
        except OSError:  # Possibly client has left the chat.
            break

# Create main GUI in memory ======================================
root = Tk()
frame=ttk.Labelframe(root,text="CLIENT")
frame.grid_configure(sticky=(N, W, E, S))
msgWin = Text(frame,wrap="word", state='normal') 
msgWin.grid(row=1,column=1,rowspan=5,columnspan=5)

# Add a ebtry box
msg = ttk.Entry(frame,width=80)
msg.bind("<Return>", OnSend)
msg.grid(row=6,column=1)
 
BtnHello = ttk.Button(frame, text = "Send>", width=20, command = OnSend)
BtnHello.grid(row=6,column=4)

# Server Socket =================================================
HOST='127.0.0.1'    # Server IP
PORT=59000          # Server Port
CLT_SocketObj = socket.socket()  # Create Socket Object

CLT_SocketObj.connect((HOST,PORT))

receive_thread = Thread(target=OnReceive)
receive_thread.start()

root.mainloop() # Starts GUI execution.
CLT_SocketObj.close()