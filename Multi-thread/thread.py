import threading
import time

exitFlag = 0

def print_time(threadName, counter, delay):
   while counter:
      if exitFlag:
         threadName.exit()
      time.sleep(delay)
      print ("%s: %s" % (threadName, time.ctime(time.time())))
      counter -= 1

class myThread (threading.Thread):
   def __init__(self, threadID, name, counter):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.counter = counter
   def run(self):
      print ("Starting " + self.name)
      print_time(self.name, 5, self.counter)
      print ("Exiting " + self.name)
      


# Create new threads
thread1 = myThread(1, "Thread-1", 1)
thread2 = myThread(2, "Thread-2", 2)

# Start new Threads
thread1.start()
thread2.start()

print ("Exiting Main Thread")


'''
import time
from threading import Thread

def ThreadFun(i):
    print "thread %d sleeps for 5 seconds" % i
    time.sleep(5)
    print "thread %d woke up" % i
    
# start 10 Threads
for i in range(10):
    thrd = Thread(target=ThreadFun, args=(i,),deamon=true)
    thrd.start()
    # If a program is running Threads that are not daemons, then the program will wait for those threads to complete before it terminates. 
    # Threads that are daemons, however, are just killed wherever they are when the program is exiting.
    thrd.join()
    #To tell one thread to wait for another thread to finish, you call .join(). 
    # If you uncomment, the main thread will pause and wait for the thread x to complete running.
'''
