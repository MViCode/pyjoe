# pip install MySQL-python
import mySQLdb

# Open database connection
db = MySQLdb.connect("localhost","UserID","password","TESTDB" )

# prepare a cursor object using cursor() method
cursor = db.cursor()
# execute SQL query using execute() method.
cursor.execute("SELECT VERSION()")

# Drop table if already exists ========================================
cursor.execute("DROP TABLE IF EXISTS EMPLOYEE")

# Create table ========================================================
sql =   "CREATE TABLE EMPLOYEE ( \
        FIRST_NAME CHAR(20) NOT NULL, \
        LAST_NAME  CHAR(20), \
        AGE INT,  \
        SEX CHAR(1), \
        INCOME FLOAT )"
cursor.execute(sql)

# Insert/Update Table ========================================================
# Prepare SQL query to INSERT a record into the database.
sql =   "INSERT INTO EMPLOYEE(FIRST_NAME,LAST_NAME,AGE,SEX,INCOME) \
        VALUES ('Mac', 'Mohan', 20, 'M', 2000)"
# SQL with parameters
firstname = 'Mac'
lastname = 'Mohan'
age=20
sex='M'
income=2000
sql = "INSERT INTO EMPLOYEE(FIRST_NAME, \
       LAST_NAME, AGE, SEX, INCOME) \
       VALUES ('%s', '%s', '%d', '%c', '%d' )" % \
       (firstname, lastname, age, sex, income)
# Prepare SQL query to UPDATE required records
sql = "UPDATE EMPLOYEE SET AGE = AGE + 1 WHERE SEX = '%c'" % ('M')
try:
   # Execute the SQL command
   cursor.execute(sql)
   # Performing Transactions ============================================
   # Commit your changes in the database
   # Commit is the operation, which gives a green signal to database to finalize
   # the changes, after this operation, no change can be reverted back.
   db.commit()
except:
   # Rollback in case there is any error
   db.rollback()

# Read data ==============================================================
sql = "SELECT * FROM EMPLOYEE WHERE INCOME > '%d'" % (income)
try:
    # Execute the SQL command
    cursor.execute(sql)
    # Fetch all the rows in a list of lists.
    results = cursor.fetchall()
    for row in results:
        fname = row[0]
        lname = row[1]
        age = row[2]
        sex = row[3]
        income = row[4]
        # Now print fetched result
        print("fname=%s,lname=%s,age=%d,sex=%s,income=%d" % \
            (fname, lname, age, sex, income ))
except:
   print ("Error: unable to fecth data")

# Fetch a single row using fetchone() method.
row = cursor.fetchone()
print("name: %s"% row[0])

# Delete records ===============================================
# Prepare SQL query to DELETE required records
sql = "DELETE FROM EMPLOYEE WHERE AGE > '%d'" % (20)
try:
   # Execute the SQL command
   cursor.execute(sql)
   # Commit your changes in the database
   db.commit()
except:
   # Rollback in case there is any error
   db.rollback()

# disconnect from server
db.close()