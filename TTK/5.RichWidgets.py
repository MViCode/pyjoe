# https://tkdocs.com/tutorial/index.html
# https://www.python-course.eu/python_tkinter.php
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.python-course.eu/
# http://effbot.org/tkinterbook/

# import Libs
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from array import *
root = Tk()
#root.geometry("800x600")

# Set the "borderwidth" configuration option (which defaults to 0, so no border), 
# "relief" option specifies the border appearance: "flat" (default), "raised", "sunken", "solid", "ridge", or "groove". 
FrameContainer = ttk.Frame(root, relief="flat")
FrameContainer.grid(row=0,column=0,sticky=(N, W, E, S))
FrameContainer.grid_columnconfigure(0, weight=1)
FrameContainer.grid_rowconfigure(0,weight=1)

#Create Menu Frame for Buttons
#padding= left, top, right and bottom padding
FrameMenu = ttk.Labelframe(FrameContainer,text="Menu",padding="10 10 10 10",borderwidth=1, relief="solid")
FrameMenu.grid(row=0,column=0,rowspan=3,sticky=(N, W, E, S),padx=5)

#Create Food Frame for input Boxes
frameFood = ttk.Labelframe(FrameContainer,text="Yami",padding="10 10 10 10",borderwidth=1,relief="solid")
frameFood.grid(row=0,column=1,rowspan=3, columnspan=2,sticky=(N, W, E, S),padx=5)


# (1)Listbox ==============================================================================================================================================
FoodClassNames=("Appetizer","Soup & Salad","Entree","Sushi & Sashimi","Maki & Hand Roll","Noodle & Rice","Pizza","Desert")
FoodLib =[
    #["Appetizer","Water Melon",'watermelon.png',0,1], #"FoodClass":"Appetizer","FoodName":"Water Melon","ImagePath":"","OrderQty":0,"QtyLeft":1
    ["Appetizer","Mango Salad",'mango.png',0,1]
]

FoodClass = StringVar(value=FoodClassNames)
menuList = Listbox(FrameMenu,listvariable=FoodClass, height=33)
menuList.grid(row=0,column=0,sticky=(N, W, E, S))
# Colorize alternating lines of the listbox
for i in range(0,len(FoodClassNames),2):
    menuList.itemconfigure(i, background='#f0f0ff')


FoodName =[['mango','Watermelon','mango'],['mango','Watermelon','mango'],['mango','Watermelon','mango']]
matrixImages = []
for i in range(3):
    matrixImages.append([])
    for j in range(3):
        matrixImages[i].append(PhotoImage(file=(FoodName[i][j])+'.png'))

matrixPlace = []
for i in range(3):
    matrixPlace.append([])
    for j in range(3):
        matrixPlace[i].append(ttk.Label(frameFood))
        matrixPlace[i][j].grid(row=i,column=j)
        matrixPlace[i][j].configure(text=FoodName[i][j],image=matrixImages[i][j],compound="bottom")


'''
for i in range(3):
    for j in range(3):
        matrix[i][j].grid(row=i,column=j)
'''

def ShowFoodDetail(*args):
    idxs = menuList.curselection()
    if len(idxs)==1:
        idx = int(idxs[0])
        fdclass = FoodClassNames[idx]
        messagebox.showinfo("Information",fdclass)


menuList.bind('<<ListboxSelect>>', ShowFoodDetail)



















# Run mainloop
root.mainloop()