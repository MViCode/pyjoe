from tkinter import *
from tkinter import ttk

mainGUI = Tk()
mainGUI.geometry("800x600")

frame4sushi = ttk.Labelframe(mainGUI,text="Sushi",width="200",height="200")
frame4sushi.grid(row=1,column=0)

frame4Desert = ttk.Labelframe(mainGUI,text="Deser",width="200",height="200")
frame4Desert.grid(row=1,column=1)

labelImage = ttk.Label(frame4sushi) 
labelImage.grid(row=1,column=1)

SushiImgs=[]
SushiImgs.append(PhotoImage(file='watermelon.png'))
SushiImgs.append(PhotoImage(file='mango.png'))
SushiImgs.append(PhotoImage(file='eye.png'))

x=0
labelImage.configure(text="image",image=SushiImgs[x],compound="bottom")

def OnEyeBtnChecked():
    global x
    x = x + 1
    if x > 2:
        x=0
    labelImage.configure(text="image",image=SushiImgs[x],compound="bottom")

btn=ttk.Button(frame4sushi,text="Eye",command=OnEyeBtnChecked)
btn.grid(row=2, column=0)



mainGUI.mainloop()