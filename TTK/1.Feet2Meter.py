
# https://tkdocs.com/tutorial/index.html
# https://www.python-course.eu/python_tkinter.php
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.python-course.eu/
# http://effbot.org/tkinterbook/

from tkinter import *
from tkinter import ttk


def calculate(*args):
    try:
        value = float(feet.get())
        meters.set((0.3048 * value * 10000.0 + 0.5)/10000.0)
    except ValueError:
        pass

# Create main GUI  
root = Tk()
root.title("Feet to Meters")

# Create a frame widget, which will hold all the content of our user interface, and place that in our main window.
# The "columnconfigure"/"rowconfigure" tell Tk that if the main window is resized, the frame should expand to take up the extra space.
# If two columns have the same weight, they'll expand at the same rate; 
# if one has a weight of 1, another of 3, the latter one will expand three pixels for every one pixel added to the first.
# If weight=0, meaning don't expand to fill space.

# The frame's "padding" option lets you specify a bit of extra padding inside the frame, whether the same amount for each of the four sides, or even different for each.
# A second way is using the "ipadx" and "ipady" grid options when adding the widget. "ipadx" puts a bit of extra space to the left and right inside the widget, 
# while "ipady" adds extra space top and bottom. Note that this extra padding is within the grid cell containing the widget.
# If you want to add padding around an entire row or column, the "columnconfigure" and "rowconfigure" methods accept a "pad" option, which will do this for you.
# use "padx" and "pady" to add space outside the widget.
mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.columnconfigure(index=0, weight=1)    # index indicates which column you want to configure
mainframe.rowconfigure(index=0, weight=1)

# Strictly speaking, we could just put the other parts of our interface directly into the main root window, without the intervening content frame. 
# However, the main window isn't itself part of the "themed" widgets, so its background color wouldn't match the themed widgets we will put inside it. 
# Using a "themed" frame widget to hold the content ensures that the background is correct.
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))

#specified the global textvariable for the entry and label, so that the value can be updated automatically bi-directionally  
feet = StringVar()
meters = StringVar()

# Create Widgets
# The "sticky" option says how the widget would line up within the grid cell, using compass directions
feet_entry = ttk.Entry(mainframe, width=7, textvariable=feet)
feet_entry.grid(column=2, row=1, sticky=(W, E))

ttk.Label(mainframe, textvariable=meters).grid(column=2, row=2, sticky=(W, E))
ttk.Button(mainframe, text="Calculate", command=calculate).grid(column=3, row=3, sticky=W)

ttk.Label(mainframe, text="feet").grid(column=3, row=1, sticky=W)
ttk.Label(mainframe, text="is equivalent to").grid(column=1, row=2, sticky=E)  
ttk.Label(mainframe, text="meters").grid(column=3, row=2, sticky=W)

# The first line walks through all of the widgets that are children of our content frame, and adds a little bit of padding around each, so they aren't so scrunched together. 
# We could have added these options to each "grid" call when we first put the widgets onscreen, but this is a nice shortcut.
for child in mainframe.winfo_children(): 
    child.grid_configure(padx=5, pady=5)

feet_entry.focus()
# Bind <Return> event to calculate function so that If the user presses the Return key (Enter on Windows) anywhere within the root window,it can be called. 
# the same as if the user pressed the Calculate button.
root.bind('<Return>', calculate)


# run event loop
root.mainloop()