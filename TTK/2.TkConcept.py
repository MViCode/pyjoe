# https://tkdocs.com/tutorial/index.html
# https://www.python-course.eu/python_tkinter.php
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.python-course.eu/
# http://effbot.org/tkinterbook/
# import Libs
from tkinter import *
from tkinter import ttk


# Each separate widget is a Python object. When creating a widget, you must pass its parent as a parameter to the widget creation function.
# The only exception is the "root" window, which is the toplevel window that will contain everything else. 
# That is automatically created, and it does not have a parent. For example:

root = Tk()
mainframe = ttk.Frame(root)
mainframe.grid(row=0,column=0)

label =ttk.Label(mainframe, text="Starting...")
label.grid(row=1,column=1)
button = ttk.Button(mainframe,text="OK")
button.grid(row=1,column=2)


# For events that don't have a command callback associated with them, you can use Tk's "bind" to capture any event, 
# and then (like with callbacks) execute an arbitrary piece of code.
# It's not worth the bother of defining regular named functions for one-off callbacks,just used Python's anonymous functions via lambda. 
label.bind('<Enter>', lambda e: label.configure(text='Moved mouse inside'))
label.bind('<Leave>', lambda e: label.configure(text='Moved mouse outside'))
label.bind('<1>', lambda e: label.configure(text='Clicked left mouse button'))
label.bind('<Double-1>', lambda e: label.configure(text='Double clicked'))
label.bind('<B3-Motion>', lambda e: label.configure(text='right button drag to %d,%d' % (e.x, e.y)))

# =============== Virtual Events ======================
# Beyond the low-level operating system events like mouse clicks and window resizes, many widgets generate higher level events called virtual events.
# For example, a listbox widget will generate a "ListboxSelect" virtual event anytime the selection changes, regardless of whether that was because the user 
# clicked on an item, moved to it with the arrow keys, or whatever. This avoids the problem of setting up multiple, possibly platform-specific event bindings 
# to capture the change. Virtual events for a widget, if any, will be listed in the widget's documentation.

# =============== Multiple Bindings ======================
# Widgets can actually have a number of different event bindings trigger for a single event.
# Normally, events can be set up for: the individual widget itself, all widgets of a certain class (e.g. buttons), the toplevel window containing the widget,
# and all widgets in the application. Each of these will fire in sequence.

# Run mainloop
root.mainloop()