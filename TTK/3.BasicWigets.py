# https://tkdocs.com/tutorial/index.html
# https://www.python-course.eu/python_tkinter.php
# https://www.tutorialspoint.com/python3/python_gui_programming.htm
# https://www.python-course.eu/
# http://effbot.org/tkinterbook/

# import Libs
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from time import sleep
# (1) Root Window ======================================================================================================================================
root = Tk()
root.geometry("800x600")


# (2) Frame and PanedWindow =======================================================================================================================================
frame4Labes = ttk.Labelframe(root,text="Labels",width="200",height="200")
frame4Labes.place(x=10,y=10)

# Set the "borderwidth" configuration option (which defaults to 0, so no border), 
# "relief" option specifies the border appearance: "flat" (default), "raised", "sunken", "solid", "ridge", or "groove".
frame4Labes.configure(borderwidth=1,relief="groove")  

""" 
The frame's "padding" option lets you specify a bit of extra padding inside the frame, whether the same amount for each of the four sides, or even different for each.
A single number of "padding" for a frame specifies the same padding all the way around, 
A list of two numbers lets you specify the horizontal then the vertical padding, 
and a list of four numbers lets you specify the left, top, right and bottom padding, in that order.

A second way is using the GRID "ipadx" and "ipady"  options when adding the widget. "ipadx" puts a bit of extra space to the left and right inside the widget, 
while "ipady" adds extra space top and bottom. Note that this extra padding is within the grid cell containing the widget.
If you want to add padding around an entire row or column, the "columnconfigure" and "rowconfigure" methods accept a "pad" option, which will do this for you.
use "padx" and "pady" to add space outside the widget. 
"""
frame4Labes["padding"]="3 3 12 12" #left, top, right and bottom padding

# Strictly speaking, we could just put the other parts of our interface directly into the main root window, without the intervening content frame. 
# However, the main window isn't itself part of the "themed" widgets, so its background color wouldn't match the themed widgets we will put inside it. 
# Using a "themed" frame widget to hold the content ensures that the background is correct.
frame4Labes.grid_configure(sticky=(N, W, E, S))

#Create Label Frame for Buttons
frame4Buttons = ttk.Labelframe(root,text="Buttons",width="200",height="200")
frame4Buttons.place(x=210+10,y=10)   
frame4Buttons.configure(borderwidth=1,relief="solid",padding="3 3 12 12")  

#Create Label Frame for input Boxes
frame4Entry = ttk.Labelframe(root,text="Input Boxes",width="200",height="200")
frame4Entry.place(x=210*2+10,y=10)
frame4Entry.configure(borderwidth=1,relief="solid",padding="3 3 12 12")  

# The "slaves" method will tell you all the widgets that have been gridded inside a master, or optionally those within just a certain column or row. 
# The "info" method will give you a list of all the grid options for a widget and their values. 
# Finally, the "configure" method lets you change one or more grid options on a widget. 

#Create Label Frame for Inidcators
frame4Indicator = ttk.Labelframe(root,text="Indicator",width="400",height="200")
frame4Indicator.place(x=0,y=200+10)
frame4Indicator.configure(borderwidth=1,relief="solid",padding="3 3 12 12")  


# (3) Labels =======================================================================================================================================
# If a grid cell is larger than the widget in it, the widget will be centered within it with the empty space around it. 
# The "sticky" option can be used to change this default behavior, the compass directions can be used: 
# "n" (north, or top edge), "ne", (north-east, or top right corner), "e", "se", "s", "sw", "w", "nw" or "center"
# Value of "nsew" will expand widget and fill up the entire cell 
label = ttk.Label(frame4Labes, text='Label Demo:\nHere displays how to use labels')   #/n can wrap the text
label.grid(row=1,column=1,columnspan=2,sticky=(N, W)) 

# Most widgets have options that can control how they are displayed if they are larger than needed.
# If the box given to the label is larger than the label requires for its contents, 
# use the "anchor" option to specify what edge or corner the label should be attached to, which would leave any empty space in the opposite edge or corner. 
label.anchor("ne")

# Using the "justify" option to control text justification, can have the values "left", "center" or "right". 
# If you only have a single line of text, this is pretty much the same as just using the "anchor" option, but is more useful with multiple lines of text.
#label.configure(justify="right")
label["justify"]="right"

# Using the "font" configuration to control the font, style option to control the color/fonts and more
# Name of predefined fonts are:
# TkDefaultFont	    The default for all GUI items not otherwise specified.
# TkTextFont	    Used for entry widgets, listboxes, etc.
# TkFixedFont	    A standard fixed-width font.
# TkMenuFont	    The font used for menu items.
# TkHeadingFont	    The font typically used for column headings in lists and tables.
# TkCaptionFont	    A font for window and dialog caption bars.
# TkSmallCaptionFont	A smaller caption font for subwindows or tool dialogs
# TkIconFont	    A font for icon captions.
# TkTooltipFont	    A font for tooltips.
label["font"]="TkHeadingFont"

#The foreground (text) and background color can be changed via the "foreground" and "background" options. 
# Colors are covered in detail later, but you can specify these as either color names (e.g. "red") or hex RGB codes (e.g. "#ff340a").
label["foreground"] ="#ff340a"
label["background"] ="yellow"

#Labels also accept the "relief" option similar to frames
label.configure(relief="groove")

# have the widget monitor a textvariable so that anytime the variable changes, the label will display the new value of the variable;
resultsContents = StringVar()
labelValue = ttk.Label(frame4Labes)
labelValue.grid(row=2,column=1)
labelValue['textvariable'] = resultsContents
resultsContents.set('100')

# Display an image in a label
# "compound"  option allow use text and image at same time, Options are:
# "text" (text only), "image" (image only),"center" (text in center of image), "top" (image above text), "left", "bottom", and "right".
labelImage = ttk.Label(frame4Labes)
labelImage.grid(row=2,column=2)
name=[
    ['eye.png'],
    ['eye.png']
    ]
EyeImageFile = PhotoImage(file=name[1][0])
labelImage.configure(text="image",image=EyeImageFile,compound="bottom")


# (4) Buttons =======================================================================================================================================
def OnEyeBtnChecked():
    messagebox.showinfo("Information","Eye Button Checked!")
btn=ttk.Button(frame4Buttons,text="Eye",command=OnEyeBtnChecked)
btn.grid(row=0, column=0)

# Buttons have same "text", "textvariable" (rarely used), "image" and "compound" configuration options as labels
btn.configure(image=EyeImageFile,compound="right")

#Button state flags available to themed widgets is: "active", "disabled", "focus", "pressed", "selected", "background", "readonly", "alternate", and "invalid". 
btn["default"]="active"     # set default button by "active", otherwise "normal"
btn.state(['disabled'])            # set the disabled flag, disabling the button
btn.state(['!disabled'])           # clear the disabled flag
BtnState=btn.instate(['disabled'])          # return true if the button is disabled, else false
BtnState=btn.instate(['!disabled'])         # return true if the button is not disabled, else false
#label["text"]=BtnState
#btn.instate(['!disabled'], OnEyeBtnChecked)    # execute 'cmd' if the button is not disabled


# (5)Check Buttons ====================================================================================================================================
# A checkbutton is like a regular button, except that not only can the user press it, which will invoke a command callback, but it also holds a binary value of some kind (i.e. a toggle). 
# Checkbuttons are used all the time when a user is asked to choose between e.g. two different values for an option.

# Checkbuttons use "text", "textvariable", "image", and "compound" options control the display of the label (next to the check box itself), 
# and the "state" and "instate" methods allow you to manipulate the "disabled" state flag to enable or disable the checkbutton. 
# Similarly, the "command" option lets you specify a script to be called everytime the user toggles the checkbutton, and the "invoke" method will also execute the same callback.
def OnCheckBtnToggled(*args):
    if SoundState.get()==1:
        #messagebox.showinfo("Information","Sound On now!")
        messagebox.askyesno(message='Sound On now!',icon='question',title='Information')
    else:
        messagebox.showinfo("Information","Sound Muted!")

# Type              Possible return values
# ok (default)	    "ok"
# okcancel	        "ok" or "cancel"
# yesno	            "yes" or "no"
# yesnocancel	    "yes", "no" or "cancel"
# retrycancel	    "retry" or "cancel"
# abortretryignore	"abort", "retry" or "ignore

# The full list of possible options is shown here:
# type	        As described above.
# message	    The main message displayed inside the alert.
# detail	    If needed, a secondary message, often displayed in a smaller font under the main message.
# title	        Title for the dialog window. Not used on Mac OS X.
# icon	        Icon to show: one of "info" (default), "error", "question" or "warning".
# default	    Specify which button (e.g. "ok" or "cancel" for a "okcancel" type dialog) should be the default.
# parent	    Specify a window of your application this dialog is being posted for; this may cause the dialog to appear on top, or on Mac OS X appear as a sheet for the window.


SoundState = IntVar()
checkBtn = ttk.Checkbutton(frame4Buttons, text='Sound',variable=SoundState, onvalue=1, offvalue=0, command=OnCheckBtnToggled)
checkBtn.grid(row=1, column=0)


# (6)Radio Buttons ====================================================================================================================================
def OnRadioBtnChanged(*args):
    if phone.get()=="home":
        messagebox.showinfo("Information","Home Phone!")
    elif phone.get()=="office":
        messagebox.showinfo("Information","Office Phone!")
    elif phone.get()=="cell":
        messagebox.showinfo("Information","Cell Phone!")
    else:
        messagebox.showinfo("Information","Not a valid Phone!")
phone = StringVar()
home = ttk.Radiobutton(frame4Buttons, text='Home', variable=phone, value='home',command=OnRadioBtnChanged)
office = ttk.Radiobutton(frame4Buttons, text='Office', variable=phone, value='office',command=OnRadioBtnChanged)
cell = ttk.Radiobutton(frame4Buttons, text='Mobile', variable=phone, value='cell',command=OnRadioBtnChanged)
home.grid(row=2, column=0)
office.grid(row=3, column=0)
cell.grid(row=4, column=0)


# (6)EntryBox  ====================================================================================================================================
pwd = StringVar()
pwdEntry = ttk.Entry(frame4Entry, width=33, textvariable=pwd,show="*")
pwdEntry.grid(row=0, column=0)

def ShowPwd(event):
    pwdEntry["show"]=""
def HidePwd(event):
    pwdEntry["show"]="*"

SeeBtn = ttk.Button(frame4Entry,image=EyeImageFile)
SeeBtn.bind('<ButtonPress-1>',ShowPwd)
SeeBtn.bind('<ButtonRelease-1>', HidePwd)
SeeBtn.grid(row=0, column=1)


# (6)ComboBox Buttons ====================================================================================================================================
def OnComboCountryChange(*args):
    if ComboCountry.get()=="Canada":
        messagebox.showinfo("Information","Canada!")
    elif ComboCountry.get()=="UK":
        messagebox.showinfo("Information","UK!")
    elif ComboCountry.get()=="North Korea":
        messagebox.showinfo("Information","North Korea")
    else:
        messagebox.showinfo("Information","Not Support!")
countryvar = StringVar()
ComboCountry = ttk.Combobox(frame4Entry, width=30, textvariable=countryvar,values=("Canada","UK","North Korea"))
ComboCountry.bind('<<ComboboxSelected>>',OnComboCountryChange)
ComboCountry.grid(row=1,column=0)


# (7)Listbox ==============================================================================================================================================
FoodClassNames=("Appetizer","Soup & Salad","Entree","Sushi","Sashimi","Maki & Hand Roll","Noodle & Rice","Pizza","Desert")

FoodClass = StringVar(value=FoodClassNames)
menuList = Listbox(frame4Entry, listvariable=FoodClass,height=8, width=33)
menuList.grid(row=2,column=0,sticky=(N, W, E, S))

# Colorize alternating lines of the listbox
for i in range(0,len(FoodClassNames),2):
    menuList.itemconfigure(i, background='#f0f0ff')

# Set the initial selected item
menuList.selection_set(2)

def ShowFoodDetail(*args):
    idxs = menuList.curselection()
    if len(idxs)==1:
        idx = int(idxs[0])
        fdclass = FoodClassNames[idx]
        messagebox.showinfo("Information",fdclass)

# menuList.bind('<<ListboxSelect>>', ShowFoodDetail)  # Click
menuList.bind('<Double-1>', ShowFoodDetail)  # Double Click
menuList.bind('<Return>', ShowFoodDetail)   # Enter


# (8)Scroll Bar ==============================================================================================================================================
# Add Horizontal Scrollbar
ScrX = ttk.Scrollbar(frame4Entry, orient=HORIZONTAL, command=menuList.xview)
ScrX.grid(row=3,column=0,sticky=(W,E))

# Add Vertical Scrollbar
ScrY = ttk.Scrollbar(frame4Entry, orient=VERTICAL, command=menuList.yview)
ScrY.grid(row=2,column=1,sticky=(N,S))

# Link the scroolbars with Widget
menuList.configure(yscrollcommand=ScrY.set,xscrollcommand=ScrY.set)


# (9) Progress Bar ==============================================================================================================================================
# use mode "determinate" if relative progress towards completion can be caculated;
# mode "indeterminate" if not possible to know how far along in the task the program is, 
# but we still want to provide feedback that things are still running.
prog=IntVar()
Progress = ttk.Progressbar(frame4Indicator, orient=HORIZONTAL, variable=prog,length=400, mode='determinate',maximum=100)
Progress.grid(row=0,column=0,sticky=(W,E))
prog.set(10)
Progress.step(20) #use amount method to ge maxium
# Indeterminate mode is for when you're not able to easily know (or estimate) how far along in a long running task you actually are, 
# but still want to provide feedback to the user that the operation is still running (or that your program hasn't crashed). 
# At the start of the operation you'll just call the progressbar's "start" method, 
# and at the end of the operation, you'll call its "stop" method. 

# (10) Scale Widgets ==============================================================================================================================================

ScaleX=IntVar()
ScaleY=IntVar()

def OnXScroll(*args):
    ScaleY.set(ScaleX.get())
    prog.set(ScaleX.get())

def OnYScroll(*args):
    ScaleX.set(ScaleY.get())
    prog.set(ScaleY.get())

ScaleBarX = ttk.Scale(frame4Entry, orient=HORIZONTAL, length=200, variable=ScaleX, from_=1.0, to=100.0,command=OnXScroll)
ScaleBarX.grid(row=4,column=0,sticky=(W,E))
ScaleX.set(10)
# ScaleBar.set(20)


ScaleBarY = ttk.Scale(frame4Entry, orient=VERTICAL, length=200, variable=ScaleY, from_=1.0, to=100.0,command=OnYScroll)
ScaleBarY.grid(row=4,column=1,sticky=(N,S))
ScaleY.set(ScaleX.get())

# (11) SpinBox ==============================================================================================================================================

SpinVal = StringVar()
def OnSpinChange():
    messagebox.showinfo("Information",SpinVal.get())

# Specifying a list of values will override to "from" and "to" settings.
SpinBox = Spinbox(frame4Entry, from_=1.0, to=100.0,increment=1, textvariable=SpinVal,value=FoodClassNames, command= OnSpinChange) 
SpinBox.grid(row=5,column=0,sticky=(W,E))

# Because spinbox is not a themed widget, the "state" and "instate" methods are not available. 
# Instead, you can change its state using the "state" configuration option. This may be "normal", "disabled" to prevent any changes.


# (12) Seperator ==============================================================================================================================================

Sperator1 = ttk.Separator(frame4Entry, orient=HORIZONTAL)
Sperator2 = ttk.Separator(frame4Entry, orient=VERTICAL)
Sperator1.grid(row=6,column=0,sticky=(W,E))
Sperator2.grid(row=6,column=1,sticky=(N,S))

# (12) PanedWindow ============================================================================================================================================
# A panedwindow widget lets you stack two or more resizable widgets above and below each other (or to the left and right).
# The user can adjust the relative heights (or widths) of each pane by dragging a sash located between them. 
# Typically the widgets you're adding to a panedwindow will be frames containing many other widgets.
PanedWnd = ttk.Panedwindow(root, orient=VERTICAL)
# first pane, which would get widgets gridded into it:
f1 = ttk.Labelframe(PanedWnd, text='Pane1', width=100, height=100)
f2 = ttk.Labelframe(PanedWnd, text='Pane2', width=100, height=100)   # second pane

# Calling the "add" method will add a new pane at the end of the list of panes. 
# The "insert position subwindow" method allows you to place the pane at the given position in the list of panes (0..n-1); 
# if the pane is already managed by the panedwindow, it will be moved to the new position. 
# You can use the "forget subwindow" to remove a pane from the panedwindow; 
# you can also pass a position instead of a subwindow.
PanedWnd.add(f1)
PanedWnd.add(f2)
PanedWnd.place(x=0,y=200+10+50)

# (13) Notebook/Tab ============================================================================================================================================
Tab = ttk.Notebook(root)
frame1 = ttk.Labelframe(Tab,text="Frame1")   # first page, which would get widgets gridded into it
frame2 = ttk.Labelframe(Tab,text="Frame2")   # second page
frame3 = ttk.Labelframe(Tab,text="Frame3")   # second page

label = ttk.Label(frame1, text='Label Demo:\nHere displays how to use labels')   #/n can wrap the text
label.grid(row=1,column=1,columnspan=2,sticky=(N, W)) 

Tab.add(frame1, text='One',state='normal')
Tab.add(frame2, text='Two',state='disabled') # state can also = 'hidden'

Tab.insert(1,frame3,text='Inserted') 
# to remove a given tab, use the "forget" method, passing it either the position (0..n-1) or the tab's subwindow. 
# the list of all subwindows contained in the notebook can be retrieve via the "tabs" method.


# Change the selected tab by passing it either the tab's position or the subwindow itself as a parameter.
Tab.select(1)

# To change a tab option (like the text label on the tab or its state), 
# you can use the "tab(tabid, option=value" method (where "tabid" is again the tab's position or subwindow); 
# omit the "=value" to return the current value of the option.
Tab.tab(0,text="changed",state='normal')

Tab.place(x=210,y=200+10+50)

# (14) Tree View ============================================================================================================================================
tree = ttk.Treeview(frame3)
tree.grid(row=1,column=1,columnspan=2,sticky=(N, W)) 







# Run mainloop
root.mainloop()