'''
    "t"     - Text - Default value. Text mode
    "b"     - Binary - Binary mode (e.g. images)
    "x"     - Create - Creates the specified file, returns an error if the file exists
    "w"     - Write - Opens a file for writing, creates the file if it does not exist
    "a"     - Append - Opens a file for appending, creates the file if it does not exist
    "r"     - Read - Default value. Opens a file for reading, error if the file does not exist
    "r+"    - Open file for reading and writing. File pointer placed at beginning of the file.
    "w+"    - Same as w but also alows to read from file.
    "a+"    - same as a but also alows to read from file.
'''
#To open a text file for read purpose, use:
fl = open("hello.txt", "rt")
print(fl.readline())    # read one line at a time
print (fl.readlines(100))  # read a list of lines
for line in fl;         # read line by line
    print(line)
print(fl.read(100))        # Read 100 bytes
print(fl.read())        # Read whole file
fl.close()  # close the file

# To append data to file:
fl = open("Hello.txt", "at")
fl.write("Hello World again")
fl.close()

# to check if file exists & delete the file
import os
os.getcwd() # get current working directory
os.mkdir(D:\\test\)
os.chdir("D:\\test")

#To write to a file, Overwite from begining:
fl = open("hello.txt",mode='wt')
fl.write("Hello World")
lines_of_text = ["a line of text", "another line of text", "a third line"]
fl.writelines(lines_of_text)
fl.close()

if os.path.exists("D:\\test\hello.txt"):
    os.chdir("D:\\test")
    os.rename("hello.txt","demofile.txt")
    os.remove("demofile.txt") # delete the file
    os.rmdir("D:\\test")
else:
    print("The file does not exist") 

'''
    The tell() method of file object tells at which byte the file cursor is located.
    In seek(offset,reference_point) the reference points are:
    - 0 (the beginning of the file and is default);
    - 1 (the current position of file)
    - 2 (the end of the file).
'''
cursor=fl.tell()
fl.seek(4,cursor)